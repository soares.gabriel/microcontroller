/*
  Baseado em https://forum.arduino.cc/index.php?topic=259450.0
*/

const int ccc = 261;
const int ddd = 294;
const int e = 329;
const int f = 349;
const int g = 391;
const int gS = 415;
const int aaa = 440;
const int aS = 455;
const int bbb = 466;
const int cH = 523;
const int cSH = 554;
const int dH = 587;
const int dSH = 622;
const int eH = 659;
const int fH = 698;
const int fSH = 740;
const int gH = 784;
const int gSH = 830;
const int aH = 880;

int counter = 0;

/**
 * [beep description]
 * @param note     [description]
 * @param duration [description]
 */
void beep(int note, int duration)
{
  //Play tone on buzzerPin
  Sound_Play(note, duration);

  //Play different LED depending on value of 'counter'
  if(counter % 2 == 0)
  {
    LATBbits.LATB0 = 1; // digitalWrite(ledPin1, HIGH);
    Delay_ms(duration);
    LATBbits.LATB0 = 0; // digitalWrite(ledPin1, LOW);
  }else
  {
    LATBbits.LATB1 = 1; // digitalWrite(ledPin2, HIGH);
    Delay_ms(duration);
    LATBbits.LATB1 = 0; // digitalWrite(ledPin2, LOW);
  }

  //Stop tone on buzzerPin
  //noTone(buzzerPin);

  Delay_ms(50);

  //Increment counter
  counter++;
}

/**
 * [firstSection description]
 */
void firstSection()
{
  beep(aaa, 500);
  beep(aaa, 500);
  beep(aaa, 500);
  beep(f, 350);
  beep(cH, 150);
  beep(aaa, 500);
  beep(f, 350);
  beep(cH, 150);
  beep(aaa, 650);

  Delay_ms(500);

  beep(eH, 500);
  beep(eH, 500);
  beep(eH, 500);
  beep(fH, 350);
  beep(cH, 150);
  beep(gS, 500);
  beep(f, 350);
  beep(cH, 150);
  beep(aaa, 650);

  Delay_ms(500);
}

/**
 * [secondSection description]
 */
void secondSection()
{
  beep(aH, 500);
  beep(aaa, 300);
  beep(aaa, 150);
  beep(aH, 500);
  beep(gSH, 325);
  beep(gH, 175);
  beep(fSH, 125);
  beep(fH, 125);
  beep(fSH, 250);

  Delay_ms(325);

  beep(aS, 250);
  beep(dSH, 500);
  beep(dH, 325);
  beep(cSH, 175);
  beep(cH, 125);
  beep(bbb, 125);
  beep(cH, 250);

  Delay_ms(350);
}

int main()
{
  ADPCFG = 0xFFFF;
  TRISBbits.TRISB0 = 0; //   pinMode(ledPin1, OUTPUT);
  TRISBbits.TRISB1 = 0; //   pinMode(ledPin2, OUTPUT);
  TRISDbits.TRISD3 = 0; //   pinMode(buzzerPin, OUTPUT);

  Sound_Init(&PORTD, 3);

  while(1){
    //Play first section
    firstSection();

    //Play second section
    secondSection();

    //Variant 1
    beep(f, 250);
    beep(gS, 500);
    beep(f, 350);
    beep(aaa, 125);
    beep(cH, 500);
    beep(aaa, 375);
    beep(cH, 125);
    beep(eH, 650);

    Delay_ms(500);

    //Repeat second section
    secondSection();

    //Variant 2
    beep(f, 250);
    beep(gS, 500);
    beep(f, 375);
    beep(cH, 125);
    beep(aaa, 500);
    beep(f, 375);
    beep(cH, 125);
    beep(aaa, 650);

    Delay_ms(650);
    }
}