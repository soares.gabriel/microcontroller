/*
2. A velocidade do motor é proporcional à saída do duty cycle do PWM no pino OCx.
A frequência do PWM começará com 20 KHz e um duty cycle de 50 %, depois de 5
segundos mudará para 80% de duty cycle, 5 segundos mais e muda para 30% de
duty cycle, 5 segundos mais e muda para 50 % de duty cycle e assim.
sucessivamente. Portanto, fazer o programa que cumpra com este enunciado.
Usar o modulo output compare no modo PWM.
*/