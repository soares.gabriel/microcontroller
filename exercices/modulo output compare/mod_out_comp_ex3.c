/*
3. Fazer um programa que mude a frequência de operação do sinal PWM da seguinte
forma: 10KHz, 20KHz, 30KHz e 60KHz e observar a tensão de saída do filtro (R=1K,
C=100nF). O duty cycle para todas estas frequências é de 10%, 30%, 40%, 50%,
70%, 80% e 90%. A mudança de frequência deve ser feita toda vez que pressiona e
solta uma tecla.
*/