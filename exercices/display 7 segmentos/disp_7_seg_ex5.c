unsigned int i, j, temp;

char pass[] = {'-','-','-','-'}, stored_pass[4];

char alphabet[] = { 'A', 'B', 'C', 'D', 'E', 'F', 'H', 'I', 'L', 
'n', 'O', 'P', 'q', 'r', 'S', 'U', 'y', 'Z',
'0', '1', '2', '3', '4', '5', '6', '7', '8','9'
};

unsigned int mask(char letter){
	switch(letter){
		case 'A' : return 0x77;
		case 'a' : return 0x77;
		case 'B' : return 0x7C;
		case 'b' : return 0x7C;
		case 'C' : return 0x39;
		case 'c' : return 0x58;
		case 'd' : return 0x5E;
		case 'D' : return 0x5E;
		case 'E' : return 0x79;
		case 'e' : return 0x79;
		case 'F' : return 0x71;
		case 'f' : return 0x71;
		case 'g' : return 0x6F;
		case 'G' : return 0x6F;
		case 'h' : return 0x74;
		case 'H' : return 0x76;
		case 'i' : return 0x30;
		case 'I' : return 0x30;
		case 'l' : return 0x38;
		case 'L' : return 0x38;
		case 'n' : return 0x54;
		case 'N' : return 0x54;
		case 'o' : return 0x5C;
		case 'O' : return 0x3F;
		case 'P' : return 0x73;
		case 'p' : return 0x73;
		case 'q' : return 0x67;
		case 'Q' : return 0x67;
		case 'r' : return 0x50;
		case 'R' : return 0x50;
		case 'S' : return 0x6D;
		case 's' : return 0x6D;
		case 't' : return 0x50;
		case 'T' : return 0x31;
		case 'u' : return 0x1C;
		case 'U' : return 0x3E;
		case 'y' : return 0x6E;
		case 'Y' : return 0x6E;
		case 'Z' : return 0x5B;
		case 'z' : return 0x5B;
		case '.' : return 0x80;
		case '-' : return 0x40;
		case '_' : return 0x08;
		case '0' : return 0x3F;
		case '1' : return 0x06;
		case '2' : return 0x5B;
		case '3' : return 0x4F;
		case '4' : return 0x66;
		case '5' : return 0x6D;
		case '6' : return 0x7D;
		case '7' : return 0x07;
		case '8' : return 0x7F;
		case '9' : return 0x6F;
		default : return 0x00;
	}
}

void showOnDisplay(char *pass, char letter){
	
	int temp = 10;
	while(temp > 0){
		LATDbits.LATD0 = 1;
		LATB = mask(pass[2]);
		Delay_ms(5);
		LATDbits.LATD0 = 0;

		LATDbits.LATD2 = 1;
		LATB = mask(pass[1]);
		Delay_ms(5);
		LATDbits.LATD2 = 0;

		LATCbits.LATC14 = 1;
		LATB = mask(pass[0]);
		Delay_ms(5);
		LATCbits.LATC14 = 0;

		LATCbits.LATC13 = 1;
		LATB = mask(letter);
		Delay_ms(5);
		LATCbits.LATC13 = 0;
		
		temp--;
	}	
}

void showOnDisplayAtomic(char *pass){

	int temp = 10;
	while(temp > 0){
		LATDbits.LATD0 = 1;
		LATB = mask(pass[0]);
		Delay_ms(5);
		LATDbits.LATD0 = 0;

		LATDbits.LATD2 = 1;
		LATB = mask(pass[1]);
		Delay_ms(5);
		LATDbits.LATD2 = 0;

		LATCbits.LATC14 = 1;
		LATB = mask(pass[2]);
		Delay_ms(5);
		LATCbits.LATC14 = 0;

		LATCbits.LATC13 = 1;
		LATB = mask(pass[3]);
		Delay_ms(5);
		LATCbits.LATC13 = 0;

		temp--;
	}
}


void showOnDisplayAtomicBlink(char *pass){

	int temp = 10;
	while(temp > 0){
		
		showOnDisplayAtomic(pass);
		Delay_ms(400);
		temp--;
	}
}

void playWarningSound()
{
	LATDbits.LATD3 = 1;
	Delay_ms(30);
	LATDbits.LATD3 = 0;
}

int main (void)
{
	ADPCFG = 0xFFFF;

	TRISB = 0; //a PORTB como saída
	LATB = 0;

	/* 7 SEG DISPLAYS */
	TRISDbits.TRISD0 = 0;
	TRISDbits.TRISD2 = 0;
	TRISCbits.TRISC13 = 0;
	TRISCbits.TRISC14 = 0;

	/* BUZZER */
	TRISDbits.TRISD3 = 0;

	i = 0;
	j = 0;
	
	while(1)
	{
		if(j < 4){
			while(i < 27 && j < 4){
				if(!PORTDbits.RD1 == 1){
					i++;
				}
				showOnDisplay(&pass, alphabet[i]);

				if (!PORTFbits.RF6 == 1){
					pass[j] = alphabet[i];
					j++;
					playWarningSound();
				}
			}
		} else {
			showOnDisplayAtomicBlink(&pass);
		}
	}
}