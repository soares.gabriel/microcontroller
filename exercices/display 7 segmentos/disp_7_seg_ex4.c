char word[] = "  Ola  pessoas  ";
unsigned int i, num_2;

unsigned int mask(char letter){
        switch(letter){
                case 'A' : return 0x77;
                case 'a' : return 0x77;
                case 'B' : return 0x7C;
                case 'b' : return 0x7C;
                case 'C' : return 0x39;
                case 'c' : return 0x58;
                case 'd' : return 0x5E;
                case 'D' : return 0x5E;
                case 'E' : return 0x79;
                case 'e' : return 0x79;
                case 'F' : return 0x71;
                case 'f' : return 0x71;
                case 'g' : return 0x6F;
                case 'G' : return 0x6F;
                case 'h' : return 0x74;
                case 'H' : return 0x76;
                case 'i' : return 0x30;
                case 'l' : return 0x38;
                case 'L' : return 0x38;
                case 'n' : return 0x54;
                case 'N' : return 0x54;
                case 'o' : return 0x5C;
                case 'O' : return 0x3F;
                case 'P' : return 0x73;
                case 'p' : return 0x73;
                case 'q' : return 0x67;
                case 'Q' : return 0x67;
                case 'r' : return 0x50;
                case 'R' : return 0x50;
                case 'S' : return 0x6D;
                case 's' : return 0x6D;
                case 't' : return 0x50;
                case 'T' : return 0x31;
                case 'u' : return 0x1C;
                case 'U' : return 0x3E;
                case 'y' : return 0x6E;
                case 'Y' : return 0x6E;
                case 'Z' : return 0x5B;
                case 'z' : return 0x5B;
                case '.' : return 0x80;
                default : return 0x00;
        }
}


//************* Programa Principal *************************
int main (void)
{
        ADPCFG = 0xFFFF;

        TRISB = 0; //a PORTB como sa�da
        LATB = 0;

        TRISDbits.TRISD0 = 0;
        TRISDbits.TRISD2 = 0;
        TRISCbits.TRISC13 = 0;
        TRISCbits.TRISC14 = 0;

        do
        {
                for(i = 0; i <= 16; i++){
                        num_2 = 10;   // loop para visualiza��o
                        while(num_2 != 0){

                                LATDbits.LATD0 = 1;
                                LATB = mask(word[i]);
                                Delay_ms(5);
                                LATDbits.LATD0 = 0;

                                LATDbits.LATD2 = 1;
                                LATB = mask(word[i+1]);
                                Delay_ms(5);
                                LATDbits.LATD2 = 0;

                                LATCbits.LATC14 = 1;
                                LATB = mask(word[i+2]);
                                Delay_ms(5);
                                LATCbits.LATC14 = 0;

                                LATCbits.LATC13 = 1;
                                LATB = mask(word[i+3]);
                                Delay_ms(5);
                                LATCbits.LATC13 = 0;

                                num_2--;
                        }
                }
        } while(1);
}