/*
Exerc�cio 3. Projetar um sistema que fa�a a contagem de 0 at� 9999.
Ou seja, que quando se ligue o kit este comece a contar desde 0000 at�
9999 em intervalos de tempo de 0,25s; Fazer isto em quatro displays de
7 segmentos.
*/


//********* Vari�veis Globais *******************************
unsigned int num_1;
unsigned int num_2;
unsigned int i;

unsigned int mask(unsigned int num){
        switch(num){
                case 0 : return 0x3F;
                case 1 : return 0x06;
                case 2 : return 0x5B;
                case 3 : return 0x4F;
                case 4 : return 0x66;
                case 5 : return 0x6D;
                case 6 : return 0x7D;
                case 7 : return 0x07;
                case 8 : return 0x7F;
                case 9 : return 0x6F;
        }
}


//************* Programa Principal *************************
int main (void)
{
        ADPCFG = 0xFFFF;

        TRISB = 0; //a PORTB como sa�da
        LATB = 0;

        TRISDbits.TRISD0 = 0;
        TRISDbits.TRISD2 = 0;
        TRISCbits.TRISC13 = 0;
        TRISCbits.TRISC14 = 0;

        do
        {
                for(i = 0; i <= 9999; i++){
                        num_2 = 10;   // loop para visualiza��o
                        while(num_2 != 0){

                                // milhares
                                num_1 = i/1000;
                                LATDbits.LATD0 = 1;
                                LATB = mask(num_1);
                                Delay_ms(2);
                                LATDbits.LATD0 = 0;

                                // centenas
                                num_1 = i%1000;
                                num_1 = num_1/100;
                                LATDbits.LATD2 = 1;
                                LATB = mask(num_1);
                                Delay_ms(2);
                                LATDbits.LATD2 = 0;

                                // dezenas
                                num_1 = i%100;
                                num_1 = num_1/10;
                                LATCbits.LATC14 = 1;
                                LATB = mask(num_1);
                                Delay_ms(2);
                                LATCbits.LATC14 = 0;

                                // unidades
                                num_1 = i%10;
                                LATCbits.LATC13 = 1;
                                LATB = mask(num_1);
                                Delay_ms(2);
                                LATCbits.LATC13 = 0;

                                num_2--;
                        }
                }
        } while(1);
}