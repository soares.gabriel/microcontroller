
/*

Utilizando a tecla ligada no pino de interrupçao externa 1 fazer o seguinte: 
toda vez que a tecla seja pressionada e depois solta se deverá mostrar no display
de 7 segmentos o número de vezes que foi pressionada.

*/

unsigned int count = 0;

unsigned int mask(unsigned int num){
	switch(num){
		case 0 : return 0x3F;
		case 1 : return 0x06;
		case 2 : return 0x5B;
		case 3 : return 0x4F;
		case 4 : return 0x66;
		case 5 : return 0x6D;
		case 6 : return 0x7D;
		case 7 : return 0x07;
		case 8 : return 0x7F;
		case 9 : return 0x6F;
	}
}

void showNumOnDisplay(unsigned int i)
{
    unsigned int num_1, num_2 = 10;   // loop para visualização
    while(num_2 != 0){

        // milhares
        num_1 = i/1000;
        LATDbits.LATD0 = 1;
        LATB = mask(num_1);
        Delay_ms(2);
        LATDbits.LATD0 = 0;

        // centenas
        num_1 = i%1000;
        num_1 = num_1/100;
        LATDbits.LATD2 = 1;
        LATB = mask(num_1);
        Delay_ms(2);
        LATDbits.LATD2 = 0;

        // dezenas
        num_1 = i%100;
        num_1 = num_1/10;
        LATCbits.LATC14 = 1;
        LATB = mask(num_1);
        Delay_ms(2);
        LATCbits.LATC14 = 0;

        // unidades
        num_1 = i%10;
        LATCbits.LATC13 = 1;
        LATB = mask(num_1);
        Delay_ms(2);
        LATCbits.LATC13 = 0;

        num_2--;
    }
}

void INT0Int() iv IVT_ADDR_INT0INTERRUPT
{
    count++;
    //Delay_ms(30);
    IFS0bits.INT0IF = 0;
}


void main ()
{
    ADPCFG = 0xFFFF;
    TRISB = 0; //a PORTB como saída

    TRISDbits.TRISD0 = 0;
	TRISDbits.TRISD2 = 0;
	TRISCbits.TRISC13 = 0;
	TRISCbits.TRISC14 = 0;

    TRISE = 0x0100; //entrada INT0=RE8
    IFS0 = 0;
    //desabilita o flag desta interrupção
    IEC0bits.INT0IE = 1; //Habilitamos INT0
    INTCON2bits.INT0EP = 0; //Borda positiva
    
    while(1){
            showNumOnDisplay(count);
    }
}