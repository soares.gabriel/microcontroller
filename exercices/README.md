# Exercícios

## Índice

* [Organização de memória e portas](https://github.com/llucasmendes/microcontroladores/tree/master/exercices/organizacao%20de%20memoria%20e%20portas)
* [Display de 7 segmentos](https://github.com/llucasmendes/microcontroladores/tree/master/exercices/display%207%20segmentos)
* [Interrupções](https://github.com/llucasmendes/microcontroladores/tree/master/exercices/modulo%20interrupcoes)
* [Ultrassom](https://github.com/llucasmendes/microcontroladores/tree/master/exercices/modulo%20ultrassom)
* [Temporizadores](https://github.com/llucasmendes/microcontroladores/tree/master/exercices/timers)
* [Output Compare](https://github.com/llucasmendes/microcontroladores/tree/master/exercices/modulo%20output%20compare)
* [Motor DC](https://github.com/llucasmendes/microcontroladores/tree/master/exercices/motor%20DC)
* [Porta Serial](https://github.com/llucasmendes/microcontroladores/tree/master/exercices/porta%20serial)


