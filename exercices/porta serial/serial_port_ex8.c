/*

Exemplo 8. Escrever uma fun��o (entrada de uma string de caracteres) chamada INSTR_UART1 para receber 
caracteres de c�digo ASCII. Utilizar as fun��es INIT_UART1 e INCHR_UART1 para um baud rate de 9600.

*/

unsigned char* x;
unsigned char string[16];
int j = 0, i = 0;

//*********** Fun��o INIT_UART1(valor_baud) **********************
/*Funciona com uma freq��ncia de 8 MHz, e ativando as interrup��es de Tx e Rx
valor_baud = INT[( (FCY/Desired Baud Rate)/16) � 1] onde INT = inteiro. */
void INIT_UART1 (unsigned char valor_baud)
{
        U1BRG = valor_baud; /*Configuramos a UART, 8 bits de dados, 1 bit de parada, sem paridade */
        U1MODE = 0x0000; //Ver tabela para saber as outras configura��es
        U1STA = 0x0000;
        IPC2 = 0x0440; //A faixa de prioridade m�dia, n�o � urgente.
        IFS0bits.U1TXIF = 0; //Zerar o flag de interrup��o de Tx.
        IEC0bits.U1TXIE = 0; //Habilita interrup��o de Tx.
        IFS0bits.U1RXIF = 0; //Zerar o flag de interrup��o de Rx.
        IEC0bits.U1RXIE = 0; //Habilita interrup��o de Rx.
        U1MODEbits.USIDL = 1;
        U1MODEbits.UARTEN = 1; //E liga a UART
        U1STAbits.UTXEN = 1;
}


unsigned char INCHR_UART1()
{
    unsigned char c;
    while (!U1STAbits.URXDA); // espera enquanto o buffer de Rx est� VAZIO.
    c = U1RXREG ; // recebe caractere.
    return c;
}

unsigned char* INSTR_UART1()
{
    while(j < 256)
    {
        string[j] = INCHR_UART1();
        if(string[j] == '\r')
        {
          break;
        }
        j++;
    }
  
  string[j++] = '\0';
  return string;
}

//************** Fun��o OUTCHR_UART1(char c) ********************
  void OUTCHR_UART1(unsigned char c)
{
    while (U1STAbits.UTXBF); // espera enquanto o buffer de Tx est� cheio.
        U1TXREG = c; // escreve caractere.
}

void OUTSTR_UART1(unsigned char *s)
{
    i = 0;
    while(s[i]) // la�o at� *s == �\0�, fim da string
        OUTCHR_UART1(s[i++]); // envia o caractere e pronto para o pr�ximo
}

int main (void){

    ADPCFG = 0xFFFF; //configura a porta B (PORTB) como entradas/saidas digitais
    TRISB = 0; //a PORTB como sa�da
    
    INIT_UART1(103); //Inicializa com 9600 bps
    
    x = INSTR_UART1();
    Delay_ms(20);
    OUTSTR_UART1(x);
    
    while(1);
}