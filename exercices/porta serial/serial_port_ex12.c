unsigned char valorBaud = 0;
unsigned char m = 0;
int num = 128;

void attribute() iv IVT_ADDR_U2RXInterrupt
{
    IFS1bits.U2RXIF = 0; // clear TX interrupt flag
    m = U2RXREG; // Read Data if there is no parity or framing error
}

//*********** Fun��o INIT_UART1(valor_baud) **********************
/*Funciona com uma freq��ncia de 8 MHz, e ativando as interrup��es de Tx e Rx
valor_baud = INT[( (FCY/Desired Baud Rate)/16) � 1] onde INT = inteiro. */

void INIT_UART2(unsigned char valorBaud)
{
    U2BRG = valorBaud;
    /*Configuramos a UART, 8 bits de dados, 1 bit de parada, sem paridade */
    U2MODE = 0x0000; //Ver tabela para saber as outras configura��es
    U2STA = 0x0000;
    IPC2 = 0x0440; //A faixa de prioridade m�dia, n�o � urgente.
    IFS1bits.U2TXIF = 0; //Zerar o flag de interrup��o de Tx.
    IEC1bits.U2TXIE = 0; //Habilita interrup��o de Tx.
    IFS1bits.U2RXIF = 0; //Zerar o flag de interrup��o de Rx.
    IEC1bits.U2RXIE = 1; //Habilita interrup��o de Rx.
    U2MODEbits.USIDL = 1;
    U2MODEbits.UARTEN = 1; //E liga a UART
    U2STAbits.UTXEN = 1;
}

//************** Fun��o OUTCHR_UART1(char c) ********************
void OUTCHR_UART2(unsigned char c)
{
    while (U2STAbits.UTXBF); // espera enquanto o buffer de Tx est� cheio.
    U2TXREG = c; // escreve caractere.
}

void funcao(int m)
{
    switch(m)
    {
       case 27:
           LATB = 0;
           Delay_ms(500);
           LATB = 255;
           Delay_ms(500);
           break;
       case 68:
           if(num<=128)
           {
              LATB = num;
              Delay_ms(1000);
              num = num*2;
           }
           else
           {
              num=1;
           }
           break;
       case 69:
           if(num>=1)
           {
              LATB = num;
              Delay_ms(1000);
              num = num/2;
           }
           else
           {
              num=128;
           }
           break;
        default:
              LATB = 0;
     }
}

/************* Programa Principal *************************/
int main(void)
{
    ADPCFG = 0xFFFF; //configura a porta B (PORTB) como entradas/saidas digitais
    TRISB = 0; //a PORTB como sa�da
    INIT_UART2(103); //Inicializa com 9600 bps
    while (1)
    {
        funcao(m);
    }
}
