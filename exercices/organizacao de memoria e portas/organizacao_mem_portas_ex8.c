/*
Exercício 8: Sabendo que quando se liga o sistema
os leds estarão apagados, então com a tecla ligada
em RF6 fazer o seguinte:

        a) Quando se pressiona e solta a tecla uma primeira
        vez se ligam todos os leds

        b) Quando se pressiona e solta a tecla uma segunda
        vez se ligam os leds de 0 a 7 sequencialmente em
        intervalos de 500 ms

        c) Quando se pressiona e solta a tecla uma terceira
        vez se ligam os leds de 7 a 0 sequencialmente em
        intervalos de 500 ms

        d) Quando se pressiona e solta a tecla uma quarta
        vez se ligam em pares (0,7), (1,6), (2,5) e (3,4)a
        intervalos de 500 ms

        e) Pressionando a tecla novamente volta ao primeiro
        estágio e assim sucessivamente
*/

//********* Variáveis Globais *******************************
int ctrl = 0;
unsigned char num_ind[8] = {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80};
unsigned char num_pair[4] = {0x81, 0x42, 0x24, 0x18};
unsigned int i, j;

void all_leds_on(){
   LATB = 0xFF;
   DelayMs(500);
}

void inc_leds_on(){
    for(j = 0 ; j < 8 ; j++)
    { // Blink LED 0, 1, 2, 3, 4, 5, 6, 7
        LATB = num_ind[j] ;
        DelayMs(500);
    }
}

void dec_leds_on(){
	// for(j = 7 ; j < 0 ; j--)
	j = 7;
	while(j > 0){ // Blink LED 7, 6, 5, 4, 3, 2, 1, 0
		LATB = num_ind[j] ; // Porta Saída Leds
		j -= 1;
		Delay_ms(500);
	}	
}

void pair_leds_on(){
    for (j = 0 ; j < 4 ; j++ )
    { // Blink LED (0,7),(1,6),(2,5) e (3,4)
        LATB = num_pair[j] ;
        DelayMs(500);
    }
}

//************* Programa Principal *************************
int main (void)
{
	ADPCFG = 0xFFFF;
	TRISB = 0x00; //a PORTB como saída
	TRISFbits.TRISF6 = 0x1; //botao 1 como entrada
	LATB = 0x00;
	// Loop infinito
	while( 1 ){
	
	       if (PORTFbits.RF6 == 0)
                  ctrl += 1;
                  
               switch(ctrl){
                           case 1: all_leds_on();
                                   break;
                           case 2: inc_leds_on();
                                   break;
                           case 3: dec_leds_on();
                                   break;
                           case 4: pair_leds_on();
                                   break;
                           case 5: ctrl = 0;
                                   break;
               }
	}
}
