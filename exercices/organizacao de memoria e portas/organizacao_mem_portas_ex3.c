// Exemplo 3. Ligar e desligar os leds da porta B
// sequencialmente em intervalos de xx ms indefinidamente.


//************* Fun��o Delay microsegundos ***********
void DelayUs(unsigned int v){
	while(v != 0){
		asm nop; asm nop; asm nop; asm nop;
		asm nop; asm nop; asm nop; asm nop;
		v--;
	}
}

//************* Fun��o Delay milisegundos ***********
void DelayMs(unsigned int z){
	while(z != 0){
		DelayUs(1000);
		z--;
	}
}

unsigned char num[8] = {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80};
unsigned char j;
int i;

//************* Programa Principal *************************
int main (void)
{
        ADPCFG = 0xFFFF;
        TRISB = 0;
        // Loop infinito
        while(1)
        {
                for(j = 0 ; j < 8 ; j++ )
                { // Blink LED 0,1,2,3,4,5,6,7
                        LATB = num[j] ; // Porta Sa�da Leds
                        DelayMs(100);
                }
        }
}