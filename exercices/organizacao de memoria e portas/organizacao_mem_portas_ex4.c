// Exemplo 4. Modificar o exemplo anterior, onde se ligar�
// e desligar� os leds de dois em dois da seguinte forma,
// led0 e led7, led1 e led6, led2 e led5, led3 e led4 em
// intervalos de xx ms indefinidamente.

//************* Fun��o Delay microsegundos ***********
void DelayUs(unsigned int v){
	while(v != 0){
		asm nop; asm nop; asm nop; asm nop;
		asm nop; asm nop; asm nop; asm nop;
		v--;
	}
}

//************* Fun��o Delay milisegundos ***********
void DelayMs(unsigned int z){
	while(z != 0){
		DelayUs(1000);
		z--;
	}
}

unsigned char num[4] = {0x81, 0x42, 0x24, 0x18};
unsigned char j;
int i;

//************* Programa Principal *************************
int main (void)
{
        ADPCFG = 0xFFFF;
        TRISB = 0; //a PORTB como sa�da
        // Loop infinito
        while( 1 )
        {
                for (j = 0 ; j < 4 ; j++ )
                { // Blink LED (0,7),(1,6),(2,5) e (3,4)
                                PORTB = num[j] ; // Porta Sa�da Leds
                                DelayMs(150);
                }
        }
}