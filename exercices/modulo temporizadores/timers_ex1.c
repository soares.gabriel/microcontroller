/*
1) Gerar no pino 1 da porta B um sinal com uma frequência de 1 KHz. Duty cicle de 50%.
*/

/**
 * [Timer1Int description]
 */
void Timer1Int() iv IVT_ADDR_T1INTERRUPT ics ICS_AUTO
{
	IFS0bits.T1IF = 0;
	LATBbits.LATB0 = ~LATBbits.LATB0; //Complementamos o bit.
}


void main (void)
{
	ADPCFG = 0xFFFF; //configura a porta B (PORTB) como entradas/saidas digitais
	TRISB = 0; //a PORTB como saída
	IFS0 = 0; //Flag de interrupção do timer1
	LATB = 0;
	IEC0bits.T1IE=1; // bit 3 do registrador IEC0 habilita a interrupção do timer1
	PR1 = 8000; //O registrador de periodo PR1 é igual a 8000
	T1CON = 0x8030; //ativamos o timer1 e o Prescaler fica em 1.
	while(1); //laço infinito
}