# ELET0077 - Microcontrollers ![](https://github.com/llucasmendes/microcontroladores/blob/master/resources/img/mc.png) 

Projects developed in the Microcontrollers course (2017.2-T01). The content of this course involves an introduction to microcontrollers, in particular the dsPIC30F4011 of the family [DSPIC30F](http://www.microchip.com/design-centers/16-bit/products/dspic30f), its characteristics, pinout, memory organization, I/O ports, etc.

## Getting Started

These instructions will get you a copy of the project up and running on your local device for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

#### Hardware

* [dsPIC30F 16-bit microntroller](http://www.microchip.com/wwwproducts/en/dsPIC30F4011) - Microcontroller unit device
  * [Datasheet](http://ww1.microchip.com/downloads/en/devicedoc/70135C.pdf)
  * [Reference Manual](http://ww1.microchip.com/downloads/en/DeviceDoc/70046E.pdf)
* [PICkit™ 3 In-Circuit Debugger/Programmer](http://www.microchip.com/Developmenttools/ProductDetails.aspx?PartNO=PG164130) - Firmware management device
  * [User's Guide](http://ww1.microchip.com/downloads/en/DeviceDoc/52116A.pdf)
  * [Quick Start Guide](http://ww1.microchip.com/downloads/en/DeviceDoc/50002010B.pdf)

#### Software

* [mikroC PRO for dsPIC](https://shop.mikroe.com/mikroc-dspicpic24) - Utilities to create software systems for the dsPIC platform
  * [User Manual](http://download.mikroe.com/documents/compilers/mikroc/dspic/mikroc-dspic-manual-v100.pdf)
  * [Quick Start Guide](http://download.mikroe.com/documents/compilers/mikroc/dspic/mikroc-dspic-creating-first-project-v100.pdf)
* [PICkit™ 3 Programmer Application](http://ww1.microchip.com/downloads/en/DeviceDoc/PICkit3%20Programmer%20Application%20v3.10.zip) - Firmware recorder doftware

#### Basic circuit

<p align="center">
  <img src="https://gitlab.com/soares.gabriel/microcontroller/-/raw/master/resources/img/kit_dspic30f4011-5_schematic.png" width="750px" height="500px"/></p>

## Deployment

### Firmware load pinout connections

<p align="center">
  <img src="https://gitlab.com/soares.gabriel/microcontroller/-/raw/master/resources/img/pickit3_connections_dsPIC30F4011.png" width="350px" height="350px"/></p>

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Gabriel Soares** - *Initial work* - [golf-sierra](https://github.com/golf-sierra)
* **Lucas Mendes** - *Initial work* - [llucasmendes](https://github.com/llucasmendes)
* **Prof. Dr. Antonio Ramirez Hidalgo** - *Supervisory work* - [ahidalgo](mailto:ahidalgo@ufs.br)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc
