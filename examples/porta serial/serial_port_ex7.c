/*

Exemplo 7. Escrever um programa que entra caracteres desde o 
teclado do PC e o eco destes caracteres voltam para a tela do PC.

*/

/*
	unsigned char m;
	unsigned char atualiza = 0;
	
	//void __attribute__((interrupt, no_auto_psv)) _U1RXInterrupt(void)
	void interrupt() iv IVT_ADDR_U1RXInterrupt ics ICS_AUTO
	{
		IFS0bits.U1RXIF = 0; // clear TX interrupt flag
		m = U1RXREG; // Read Data if there is no parity or framing error
		atualiza = 1;
	}
*/

//*********** Função INIT_UART1(valor_baud) **********************
/*Funciona com uma freqüência de 8 MHz, e ativando as interrupções de Tx e Rx
valor_baud = INT[( (FCY/Desired Baud Rate)/16) – 1] onde INT = inteiro. */
void INIT_UART1 (unsigned char valor_baud)
{
	U1BRG = valor_baud; /*Configuramos a UART, 8 bits de dados, 1 bit de parada, sem paridade */
	U1MODE = 0x0000; //Ver tabela para saber as outras configurações
	U1STA = 0x0000;
	IPC2 = 0x0440; //A faixa de prioridade média, não é urgente.
	IFS0bits.U1TXIF = 0; //Zerar o flag de interrupção de Tx.
	IEC0bits.U1TXIE = 0; //Habilita interrupção de Tx.
	IFS0bits.U1RXIF = 0; //Zerar o flag de interrupção de Rx.
	IEC0bits.U1RXIE = 0; //Habilita interrupção de Rx.
	U1MODEbits.USIDL = 1;
	U1MODEbits.UARTEN = 1; //E liga a UART
	U1STAbits.UTXEN = 1;
}

//************** Função INCHR_UART1(char c) ********************
unsigned char INCHR_UART1()
{
	unsigned char c;
	while (!U1STAbits.URXDA); // espera enquanto o buffer de Rx está VAZIO.
	c = U1RXREG ; // recebe caractere.
	return c;
}

//************** Função OUTCHR_UART1(char c) ********************
void OUTCHR_UART1(unsigned char c)
{
	while (U1STAbits.UTXBF); // espera enquanto o buffer de Tx está cheio.
	U1TXREG = c; // escreve caractere.
}

/************* Programa Principal *************************/
int main (void)
{
	ADPCFG = 0xFFFF; //configura a porta B (PORTB) como entradas/saidas digitais
	TRISB=0; //a PORTB como saída
	INIT_UART1(103); //Inicializa com 9600 bps
	unsigned char y;
	while (1)
	{
		y = INCHR_UART1();
		OUTCHR_UART1(y);
		
		/*
		
		if (atualiza == 1)
		{
			OUTCHR_UART1(m);
			OUTCHR_UART1('\n');
			OUTCHR_UART1('\r');
			atualiza = 0;
		}

		*/
	}
}