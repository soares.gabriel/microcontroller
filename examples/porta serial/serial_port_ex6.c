/*
Exemplo 6. Fazer um programa que mande pela porta serial o seguinte número 123,456. Utilizar o exemplo 5.
Solução igual a 5.
*/

/*
Exemplo 5. Escrever uma função (saída de uma string de caracteres) chamada 
OUTSTR_UART1 para transmitir caracteres de código ASCII. Utilizar as funções 
INIT_UART1 e OUTCHR_UART1 para um baud rate de 9600. Seguir configuração da 
UART do item 1.
*/

unsigned char cadena[] ="123,456\0";

//*********** Função INIT_UART1(valor_baud) **********************
void INIT_UART1 (valor_baud)
{
	U1BRG = valor_baud;
	/*Configuramos a UART, 8 bits de dados, 1 bit de parada,
	sem paridade */
	U1MODE = 0x0000; //Ver tabela para saber as outras configurações
	U1STA = 0x0000;
	IPC2 = 0x0440; //A faixa de prioridade média, não é urgente.
	IFS0bits.U1TXIF = 0; //Zerar o flag de interrupção de Tx.
	IEC0bits.U1TXIE = 0; //Habilita interrupção de Tx.
	IFS0bits.U1RXIF = 0; //Zerar o flag de interrupção de Rx.
	IEC0bits.U1RXIE = 0; //Habilita interrupção de Rx.
	U1MODEbits.UARTEN = 1; //E liga a UART
	U1STAbits.UTXEN = 1;
}

//************** Função OUTCHR_UART1(char c) ********************
void OUTCHR_UART1(unsigned char c)
{
	while ( U1STAbits.UTXBF); // espera enquanto o buffer de Tx está cheio.
	U1TXREG = c; // escreve caractere.
}

//************* Função OUTSTR_UART1(char *s) **********************
void OUTSTR_UART1(unsigned char *s)
{
	unsigned int i=0;
	while(s[i]) // laço até *s == ‘\0’, fim da string
	OUTCHR_UART1(s[i++]); // envia o caractere e pronto para o próximo
}

/************* Programa Principal *************************/
int main (void)
{
	INIT_UART1(103); //Inicializa com 9600 bps
	OUTSTR_UART1(cadena); //Escrevendo a string.
	while (1);
}