/* Exerc�cio 1. Escrever uma sequ�ncia de instru��es para
inicializar a porta serial (UART1) e funcionar como uma UART
de 8 bits de dados, sem paridade e 1 bit de parada a 2400 baud.
Fazer uma fun��o chamada INIT_UART. Levar em considera��o que
a frequ�ncia de rel�gio Fclk = 8MHz e a Fcy = 8MHz */

//===============================================================
//*********** Fun��o INIT_UART1(valor_baud) **********************
/*Funciona com uma freq��ncia de 8 MHz, e ativando as interrup��es
de Tx e Rx
valor_baud = INT[( (FCY/Desired Baud Rate)/16) � 1]
onde INT = inteiro. */
void INIT_UART1 (unsigned int valor_baud)
{
	U1BRG = valor_baud;
	/*Configuramos a UART, 8 bits de dados, 1 bit de parada,
	sem paridade */
	U1MODE = 0x0000; //Ver tabela para saber as outras configura��es
	U1STA = 0x0000;
	IPC2 = 0x0440; //A faixa de prioridade m�dia, n�o � urgente.
	IFS0bits.U1TXIF = 0; //Zerar o flag de interrup��o de Tx.
	IEC0bits.U1TXIE = 1; //Habilita interrup��o de Tx.
	IFS0bits.U1RXIF = 0; //Zerar o flag de interrup��o de Rx.
	IEC0bits.U1RXIE = 1; //Habilita interrup��o de Rx.
	U1MODEbits.UARTEN = 1; //E liga a UART
	U1STAbits.UTXEN = 1;
}

/************* Programa Principal *************************/
void main (void)
{
	/*

	Encontramos o valor do registrador U1BRG, Calculando o baud rate desejado = 2400 bps.

	Baud Rate = FCY/(16 (U1BRG + 1))
	U1BRG = ( (FCY/Desired Baud Rate)/16) � 1
	U1BRG = ((8000000/2400)/16) � 1
	U1BRG = [207,33] = 207

	*/

	U1BRG = 207; //8MHz , 2400 bps

	/*Configuramos a UART, 8 bits de dados, 1 bit de parada, sem paridade */
	U1MODE = 0x0000; //Ver tabela para saber as outras configura��es
	U1STA = 0x0000;
	IPC2 = 0x0440; //A faixa de prioridade m�dia, n�o � urgente.
	IFS0bits.U1TXIF = 0; //Zerar o flag de interrup��o de Tx.
	IEC0bits.U1TXIE = 1; //Habilita interrup��o de Tx.
	IFS0bits.U1RXIF = 0; //Zerar o flag de interrup��o de Rx.
	IEC0bits.U1RXIE = 1; //Habilita interrup��o de Rx.
	U1MODEbits.UARTEN = 1; //E liga a UART
	U1STAbits.UTXEN = 1;
}