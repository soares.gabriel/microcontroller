/* Exemplo 4. Fazer o mesmo que no exemplo 3 s� que com interrup��es */

unsigned char m;
unsigned char atualiza=0;

//void __attribute__((interrupt, no_auto_psv)) _U1RXInterrupt(void)
void interrupt() iv IVT_ADDR_U1RXInterrupt ics ICS_AUTO
{
	IFS0bits.U1RXIF = 0; // Limpa o flag de interrup��o de TX
	m = U1RXREG; // Le o dado do buffer e o pasa para a vari�vel m
	atualiza = 1; // flag para saber que aconteceu a interrup��o.
}

void INIT_UART1 (unsigned char valor_baud)
{
	U1BRG = valor_baud;
	/*Configuramos a UART, 8 bits de dados, 1 bit de parada, sem paridade */
	U1MODE = 0x0000; //Ver tabela para saber as outras configura��es
	U1STA = 0x0000;
	IPC2 = 0x0440; //A faixa de prioridade m�dia, n�o � urgente.
	IFS0bits.U1TXIF = 0; //Zerar o flag de interrup��o de Tx.
	IEC0bits.U1TXIE = 0; //Habilita interrup��o de Tx.
	IFS0bits.U1RXIF = 0; //Zerar o flag de interrup��o de Rx.
	IEC0bits.U1RXIE = 1; //Habilita interrup��o de Rx.
	U1MODEbits.USIDL = 1;
	U1MODEbits.UARTEN = 1; //E liga a UART
	U1STAbits.UTXEN = 1;
}

/************* Programa Principal *************************/
int main (void)
{
	ADPCFG = 0xFFFF; //configura a porta B (PORTB) como entradas/saidas digitais
	TRISB=0; //a PORTB como sa�da
	INIT_UART1(103); //Inicializa com 9600 bps
	while (1)
	{
		if (atualiza == 1)
		{
			LATB = m;
			atualiza = 0;
		}
	}
}