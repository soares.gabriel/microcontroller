/*
Mantendo em mente que desejamos ser capazes de produzir um sinal
PWM com no mínimo 40 KHz de frequência, e considerando que
usamos um Fxtal = 8MHz, PLL = 8 teremos um Fcy = 16MHz, então
calculamos um valor para o registrador de período de algum timer
PRx da equação do período do sinal PWM visto anteriormente:

	TPWM = [(PRx) + 1] * Tcy * (Valor do prescaler do TMRx) 
	25 us = (PRx + 1) * 62,5 ns * 1 ==> PRx = 400 – 1

Gerar um sinal PWM de 40KHz com duty cycle de 50%.
*/

void T3Interrupt() iv IVT_ADDR_T3INTERRUPT ics ICS_AUTO
{
	IFS0bits.T3IF = 0; //Limpamos o flag e saimos
}

void IniciaAudio (void)
{
	//Inicia o PWM
	//configura o duty cycle inicial (mestre e escravo)
	OC1R = OC1RS = 200;
	//inicia com duty cycle de 50 %
	//Ativa o modulo PWM
	//Selecionamos timer 3, modo PWM em OC1 e pino de falha desabilitado
	OC1CON = 0x000E;
	//Inicializa o timer 3 para fornecer a base de tempo
	//(400 - 1) configura o período (o duty cycle máximo)
	PR3 = 399;
	IFS0bits.T3IF = 0; //limpa o flag de interrupção
	IEC0bits.T3IE = 1; //habilita interrupção do timer3
	T3CON = 0x8000; //habilita TMR3, prescaler 1:1, clock interno
}

// IniciaAudio
int main (void)
{
	_TRISD0 = 0;
	IniciaAudio();
	//laco principal
	while(1);
}
