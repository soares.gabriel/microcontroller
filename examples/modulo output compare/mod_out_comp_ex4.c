/* Mantendo em mente que desejamos ser capazes de produzir um
sinal PWM com no mínimo 40 KHz de frequência, e considerando que
usamos um Fxtal = 8MHz, PLL = 8 teremos um Fcy = 16MHz, então
calculamos um valor para o registrador de período de algum timer
PRx da equação do período do sinal PWM visto anteriormente:
	
	TPWM = [(PRx) + 1] * Tcy * (Valor do prescaler do TMRx)
	25 us = (PRx + 1) * 62,5 ns * 1 ==> PRx = 400 – 1

Vamos gerar uma onda senoidal de 400Hz. Fazemos isto mudando a
programação do ISR do temporizador. */


#include <math.h>

unsigned int Sample=0; //declarando e inicializando um contador

const int Table[100] =
{
200,212,225,237,249,261,273,285,296,307,317,327,336,345,354,361,368,37
5,380,385,390,393,396,398,399,399,399,398,396,393,390,386,381,375,368,
361,354,345,337,327,317,307,296,285,273,262,250,237,225,212,200,187,17
5,162,150,138,126,115,103,93,82,72,63,54,46,38,31,24,19,14,9,6,3,1,0,0
,0,2,3,6,9,13,18,24,30,37,45,53,62,72,81,92,103,114,125,137,149,161,17
4,186
};

void T3Interrupt() iv IVT_ADDR_T3INTERRUPT ics ICS_AUTO
{
	OC1RS = Table[Sample];
	
	if(++Sample >= 100)
	{
		Sample = 0;
	}
	
	IFS0bits.T3IF = 0; //Limpamos o flag e saimos
}
void IniciaAudio (void)
{
	//Inicia o PWM,configura o duty cycle inicial (mestre e escravo)
	OC1R = OC1RS = 200;	//inicia com duty cycle de 50 %
	//Ativa o modulo PWM
	OC1CON = 0x000E; //timer 3, modo PWM em OC1 e sem pino de falha
	//Inicializa o timer 3 para fornecer a base de tempo
	PR3 = 399;	//(400 - 1)configura o período (o duty cycle máximo)
	IFS0bits.T3IF = 0;	//limpa o flag de interrupção
	IEC0bits.T3IE = 1;	//habilita interrupção do timer3
	T3CON = 0x8000;	//habilita TMR3, prescaler 1:1, clock interno
} // IniciaAudio

int main (void)
{
	TRISDbits.TRISD0 = 0; //pino 0 da porta D como saída
	IniciaAudio();
	//laco principal
	while(1) ;
} // main
