// Exemplo 1. Escrever 0x0F na porta B.
//************* Programa Principal *************************
int main ()
{
        ADPCFG = 0xFFFF;//configura a porta B (PORTB)
        // como entradas/saidas digitais
        TRISB = 0; //a PORTB como saia
        while (1){
                LATB = 0x0F;
        }
}