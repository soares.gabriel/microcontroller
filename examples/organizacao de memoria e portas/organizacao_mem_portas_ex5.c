// Exemplo 5. com a tecla ligada a RF6 do kit fazer o seguinte:
// quando RF6 est� em 1 se ligam todos os leds na porta B
// e quando RF6 est� em 0 desligar os leds.

//********* Vari�veis Globais *******************************

//************* Programa Principal *************************
int main (void)
{
	ADPCFG = 0xFFFF;
	TRISB = 0x00; //a PORTB como sa�da
	TRISFbits.TRISF6 = 0x1; //botao 1 como entrada
	LATB = 0x00;
	// Loop infinito
	while( 1 )
	{
		if(PORTFbits.RF6 == 0x1)
			LATB = 0xFF; // Porta Sa�da Leds
		if(PORTFbits.RF6 == 0x0)
			LATB = 0x00;
	}
}