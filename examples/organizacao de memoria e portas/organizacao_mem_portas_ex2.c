// Exemplo 2.Ligar e desligar os leds na porta B,
// com um atraso de tempo.

// //************* Fun��o Delay microsegundos ***********
// void DelayUs(unsigned int v){
// 	while(v != 0){
// 		asm nop; asm nop; asm nop; asm nop;
// 		asm nop; asm nop; asm nop; asm nop;
// 		v--;
// 	}
// }

// //************* Fun��o Delay milisegundos ***********
// void DelayMs(unsigned int z){
// 	while(z != 0){
// 		DelayUs(1000);
// 		z--;
// 	}
// }

//************* Programa Principal *************************
int main (){
	ADPCFG = 0xFFFF;
	TRISB = 0;
	while (1){
		LATB = 0xFF;
                Delay_ms(1000); 
                LATB = 0x00;
		Delay_ms(1000);
	}
}