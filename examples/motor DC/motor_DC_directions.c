int counter;

void dirLeft()
{
        /* left motor (black-red): onForward */
        LATFbits.LATF0 = 1;
        LATFbits.LATF1 = 0;
        
        /* right motor (yellow-green): onReverse */
        LATFbits.LATF4 = 1;
        LATFbits.LATF5 = 0;
}

void dirRight()
{
        /* left motor (black-red): onReverse */
        LATFbits.LATF0 = 0;
        LATFbits.LATF1 = 1;
        
        /* right motor (yellow-green): onForward */
        LATFbits.LATF4 = 0;
        LATFbits.LATF5 = 1;
}

void dirForward()
{
        /* left motor (black-red): onForward */
        LATFbits.LATF0 = 0;
        LATFbits.LATF1 = 1;
        
        /* right motor (yellow-green): onForward */
        LATFbits.LATF4 = 1;
        LATFbits.LATF5 = 0;
}

void dirReverse()
{
        /* left motor (black-red): onReverse */
        LATFbits.LATF0 = 1;
        LATFbits.LATF1 = 0;
        
        /* right motor (yellow-green): onReverse */
        LATFbits.LATF4 = 0;
        LATFbits.LATF5 = 1;
}

void onHold()
{
        /* left motor (black-red): stop */
        LATFbits.LATF0 = 0;
        LATFbits.LATF1 = 0;
        
        /* right motor (yellow-green): stop */
        LATFbits.LATF4 = 0;
        LATFbits.LATF5 = 0;
}


void main (void)
{
        TRISF = 0; /* a PORTF como sa�da */
        LATF = 0;  /* zerar a PORTF */

        while(1)
        {
                dirRight();
                Delay_ms(1000);
                dirLeft();
                Delay_ms(1000);
                dirForward();
                Delay_ms(5000);
                dirReverse();
                Delay_ms(5000);
                onHold();
                Delay_ms(1000);
        }
}