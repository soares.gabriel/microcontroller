// 3. Utilizando a tabela de vetores de interrupção, escrever um programa que inclua o
// ISR (só o cabeçalho) para uma interrupção externa 0 que se ativa na borda de
// subida.

void INT0Int() iv IVT_ADDR_INT0INTERRUPT
{
	/*... */
	IFS0bits.INT0IF = 0;
}

void main (void)
{
	IFS0 = 0; //aqui se encontra o bit de flag de status da interrupção Externa 0
	IEC0bits.INT0IE = 1; //Habilita a interrupção externa 0
	INTCON2bits.INT0EP = 0; //Se ativa na borda positiva (subida)
}