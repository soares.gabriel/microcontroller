// 5. Toda vez que pressionar e soltar a tecla ligada ao pino onde se encontra a entrada
// da interrupção externa 0, a porta B ligará e desligara todos os leds com um
// intervalo de tempo de 250 ms. Quando se pressionar e soltar novamente o piscar
// dos leds para e assim sucessivamente. Fazer o programa que execute este
// enunciado usando as interrupções.

unsigned char cont = 0;
//************* ISR da interrupção externa 0 *
void INT0Int() iv IVT_ADDR_INT0INTERRUPT
{
	cont++;
	IFS0bits.INT0IF = 0;
}

//** Programa Principal ******
void main ()
{
	ADPCFG = 0xFFFF;
	TRISB=0; //a PORTB como saída
	TRISE = 0x0100; //entrada INT0=RE8
	IFS0 = 0;
	IEC0bits.INT0IE = 1;//Habilita INT0
	INTCON2bits.INT0EP = 0; //Borda positiva
	while(1)
	{
		if (cont == 1)
		{
			LATB = 255;
			Delay_ms(250);
			LATB = 0;
			Delay_ms(250);
		}
		if (cont ==0 || cont > 1)
		{
			LATB = 0;
			cont = 0;
		}
	}
}