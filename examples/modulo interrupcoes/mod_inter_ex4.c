// 4. Toda vez que pressionar e soltar a tecla ligada ao pino onde se encontra a entrada
// da interrupção externa 0, na porta B onde estão ligados os leds deve aparecer a
// contagem do número de vezes que foi pressionada a tecla. Fazer o programa que
// execute este enunciado usando as interrupções.

//************* ISR da interrupção externa 0 *
void INT0Int() iv IVT_ADDR_INT0INTERRUPT
{
	LATB++;
	IFS0bits.INT0IF = 0;
}

//************* Programa Principal ***********

void main ()
{
	ADPCFG = 0xFFFF;
	TRISB=0; //a PORTB como saída
	TRISE = 0x0100; //entrada INT0=RE8
	IFS0 = 0;
	//desabilita o flag desta interrupção
	IEC0bits.INT0IE = 1; //Habilitamos INT0
	INTCON2bits.INT0EP = 0; //Borda positiva
	while(1) ; // Loop infinito
}