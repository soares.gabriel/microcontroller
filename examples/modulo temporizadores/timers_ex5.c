/*
Exemplo: Contar cada 800 pulsos e incrementar o valor da porta B. Neste exemplo o timer1 é usado para
contar a cada oitavo pulso de um clock externo no pino externo T1CK. Depois de 100 pulsos a rotina de
interrupção é chamada e o valor da porta B é incrementada.
*/

void __attribute__ ((interrupt, no_auto_psv)) _T1Interrupt(void)
{
	IFS0bits.T1IF = 0;
	LATB++; //incrementamos o valor da porta B.
}

//************* Programa Principal *************************
void main (void)
{
	ADPCFG = 0xFFFF;
	//configura a porta B (PORTB) como entradas/saidas digitais
	TRISB=0;
	//a PORTB como saída
	TRISC = 0x4000;
	// PORTC<14>=1; pino T1CKde entrada
	IFS0=0;
	//Flag de interrupção do timer1
	LATB=0;
	IEC0 = IEC0 | 0x0008; // bit 3 do registrador IEC0 habilita a interrupção do timer1 (IEC0bits.T1IE=1)
	PR1 = 100; //O registrador de período PR1 é igual a 100, quando TMR1 é igual a PR1 se ativa a interrupção
	T1CON=0x8012; //ativamos o timer1 como contador assíncrono, o Prescaler fica em 8 ou seja o TMR1
	// contará cada 8 pulsos e ativamos TCS(T1CK)
	while(1); //laço infinito
}