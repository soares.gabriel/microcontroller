/*
Escrever um programa usando o Timer 1 para criar uma onda 
quadrada de 10 KHz no pino 0 da porta B. Sabendo que a frequência 
do cristal é de 8MHz, se utiliza o PLL x 8 e o prescaler do timer é 1.
*/

/*
Solução:

Como precisamos uma saída de 10 KHZ o período deste sinal será de T = 100 useg. 
Em ALTO 50 useg e em BAIXO outros 50 useg. Portanto, temos que ter um delay de 
50 useg, para isso devemos fazer os seguintes cálculos:

	Tcy = 1/MIPS ; MIPS = (Fosc * PLLx)/4; T(50) = tempo de 50 useg; 
	T(50) = Tcy * PR1. 

Devemos encontrar o valor de PR1. Então 
	
	PR1 = T(50)/Tcy = T(50) * (Fosc * PLLx)/4 = (50 useg) * (8 MHz * 8)/4 = 800
*/

//************* Programa Principal *************************
void main (void)
{
	ADPCFG = 0xFFFF;
	//configura a porta B (PORTB) como entradas/saidas digitais
	TRISB=0;
	//a PORTB como saída
	IFS0=0;
	//Flag de interrupção do timer1
	LATB=0;
	PR1 = 800; //O registrador de periodo PR1 é igual a 800
	T1CON=0x8000; //ativamos o timer1 e o Prescaler fica em 1.
	while(1)
	{
		while (IFS0bits.T1IF ==0); //Fica nessa instrução até que esse bit mude para 1
		IFS0bits.T1IF = 0;
		LATBbits.LATB0 = ~LATBbits.LATB0; //Complementamos o bit.
	}
}