// HOLA deslocando pra direita

#define DISPLAYS_OUT LATB
#define DISPLAY_3 LATCbits.LATC13
#define DISPLAY_4 LATCbits.LATC14
#define DISPLAY_2 LATDbits.LATD2
#define DISPLAY_1 LATDbits.LATD0

unsigned int i;
char content[] = {"HOLA"}, content1[] = {"HOLA"};
unsigned int vezes,cont=0,botao=0;

void INT1Int() iv IVT_ADDR_INT1INTERRUPT
{
  cont++;
  delay_ms(10);
  IFS0bits.INT0IF = 0;
}

unsigned int mask(char letter){
        switch(letter){
                case 'A' : return 0x77;
                case 'a' : return 0x77;
                case 'B' : return 0x7C;
                case 'b' : return 0x7C;
                case 'C' : return 0x39;
                case 'c' : return 0x58;
                case 'd' : return 0x5E;
                case 'D' : return 0x5E;
                case 'E' : return 0x79;
                case 'e' : return 0x79;
                case 'F' : return 0x71;
                case 'f' : return 0x71;
                case 'g' : return 0x6F;
                case 'G' : return 0x6F;
                case 'h' : return 0x74;
                case 'H' : return 0x76;
                case 'i' : return 0x30;
                case 'I' : return 0x30;
                case 'l' : return 0x38;
                case 'L' : return 0x38;
                case 'n' : return 0x54;
                case 'N' : return 0x54;
                case 'o' : return 0x5C;
                case 'O' : return 0x3F;
                case 'P' : return 0x73;
                case 'p' : return 0x73;
                case 'q' : return 0x67;
                case 'Q' : return 0x67;
                case 'r' : return 0x50;
                case 'R' : return 0x50;
                case 'S' : return 0x6D;
                case 's' : return 0x6D;
                case 't' : return 0x78;
                case 'T' : return 0x31;
                case 'u' : return 0x1C;
                case 'U' : return 0x3E;
                case 'y' : return 0x6E;
                case 'Y' : return 0x6E;
                case 'Z' : return 0x5B;
                case 'z' : return 0x5B;
                case '.' : return 0x80;
                case '-' : return 0x40;
                case '_' : return 0x08;
                case '0' : return 0x3F;
                case '1' : return 0x06;
                case '2' : return 0x5B;
                case '3' : return 0x4F;
                case '4' : return 0x66;
                case '5' : return 0x6D;
                case '6' : return 0x7D;
                case '7' : return 0x07;
                case '8' : return 0x7F;
                case '9' : return 0x6F;
                default : return 0x00;
        }
}

void main() {

      ADPCFG = 0xFFFF; //programa todos os pinos I/O como digitais

      TRISB=0; //a PORTB como sa�da
      TRISEbits.TRISE8 = 1;
      TRISCbits.TRISC13=0;
      TRISCbits.TRISC14=0;  //DISPAY
      TRISDbits.TRISD2=0;
      TRISDbits.TRISD0=0;
      
      DISPLAY_1  = 0;  //
      DISPLAY_2  = 0;  //   Display Apagado
      DISPLAY_3 =  0;  //
      DISPLAY_4 =  0;  //


      IFS0 = 0; //desabilita o flag desta interrup��o
      IEC0bits.INT0IE = 1; //Habilitamos INT0
      INTCON2bits.INT0EP = 0;

      content[0] = 0;   //
      content[1] = 1;   //  HOLA Centralizado
      content[2] = 2;   //
      content[3] = 3;   //
      
        while(1){
        for(i = 0 ; i < 5 ; i++){
          vezes = 80;  //quanto menor , mais r�pido ele vai passar
                      //no display.

                  while(vezes != 0)
                  {

                        DISPLAY_1 = 0;
                        DISPLAYS_OUT = mask(content[3]);
                        Delay_ms(2);
                        DISPLAY_1 = 1;

                        DISPLAY_2 = 0;
                        DISPLAYS_OUT = mask(content[2]);
                        Delay_ms(2);
                        DISPLAY_2 = 1;

                        DISPLAY_3 = 0;
                        DISPLAYS_OUT = mask(content[1]);
                        Delay_ms(2);
                        DISPLAY_3 = 1;

                        DISPLAY_4 = 0;
                        DISPLAYS_OUT = mask(content[0]);
                        Delay_ms(2);
                        DISPLAY_4 = 1;

                        vezes --;
                              if(vezes == 0){
                                  if((cont%2==0) && botao!=0)botao=1;
                                  else botao=2;
                                  if(cont==0) botao=0;
                                  
                                  switch(botao) {
                                  case 1:
                                   content[0] = content1[4];  //
                                   content[1] = content1[0];  //
                                   content[2] = content1[1];  // Desloca  DIREITA
                                   content[3] = content1[2];  //
                                   content[4] = content1[3];  //
                                   break;

                                  case 2:
                                   content[0] = content1[1];  //
                                   content[1] = content1[2];  //
                                   content[2] = content1[3];  // Desloca  ESQUERDA
                                   content[3] = content1[4];  //
                                   content[4] = content1[0];  //
                                   break;

                                   break;
                                  case 0:
                                    content[0] = 0;   //
                                    content[1] = 1;   //  HOLA Centralizado
                                    content[2] = 2;   //
                                    content[3] = 3;   //
                                    break;
                                   }
                    content1[4] = content[4]; //
                    content1[3] = content[3]; //
                    content1[2] = content[2]; //  Salva onde tava
                    content1[1] = content[1]; //
                    content1[0] = content[0]; //
                                  }
                  }
          }
   }
}