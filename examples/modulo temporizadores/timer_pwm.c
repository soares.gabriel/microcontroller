/*
Escrever um programa usando o Timer 1 para criar uma onda
quadrada de 1 KHz no pino 0 da porta B. Sabendo que a
frequ�ncia do cristal � de 8MHz, se utiliza o PLL x8 e
o prescaler do timer � 1. Usar interrup��es (Timer1).
*/

unsigned int contador, dutyCycle;

void T3Interrupt() iv IVT_ADDR_T3INTERRUPT ics ICS_AUTO {

     IFS0bits.T3IF = 0;
        if(contador > 100 - dutyCycle)
            LATBbits.LATB0 = 1;
         else
                 LATBbits.LATB0 = 0;


         if(contador > 99)
            contador = 1;
         else
                 contador++;
}


void main (void)
{
        ADPCFG = 0xFFFF; //configura a porta B (PORTB) como entradas/saidas digitais
        TRISB = 0; //a PORTB como sa�da
        IFS0 = 0; //Flag de interrup��o do timer1
        LATB = 0;

        IEC0bits.T3IE = 1;
        PR2 = 57860;
        PR3 = 0x004;
        T3CON = 0x8000; //ativamos o timer1 e o Prescaler fica em 1.
        
        while(1){
                 dutyCycle = 5;
                 Delay_ms(500);
                 dutyCycle = 12;
                 Delay_ms(500);
        }
}