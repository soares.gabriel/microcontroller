/*
Exemplo: Usar o timer1 no modo gated time accumulation. Habilitar GATE o sinal é aplicado ao pino
T1CK. Medir a largura do nivel ALTO do sinal e mostrar o resultado na porta B.
*/

void __attribute__ ((interrupt, no_auto_psv)) _T1Interrupt(void)
{
	IFS0bits.T1IF = 0;
	LATB = TMR1; //mostra-se na porta B a duração do pulso.
}

//************* Programa Principal *************************
void main (void)
{
	ADPCFG = 0xFFFF;
	//configura a porta B (PORTB) como entradas/saídas digitais
	TRISB=0;
	//a PORTB como saída
	TRISC = 0x4000;
	// PORTC<14>=1; pino T1CK de entrada
	IFS0=0;
	//Flag de interrupção do timer1
	LATB=0;
	IEC0 = IEC0 | 0x0008; // bit 3 do registrador IEC0 habilita a interrupção do timer1 (IEC0bits.T1IE=1)
	PR1 =0xFFFF;
	//O registrador de período PR1 é igual a FFFF
	T1CON=0x8040; //ativamos o timer1 como contador síncrono, ativamos TCS(T1CK) e TGATE
	while(1); //laço infinito
}

/* O valor de PR1 é grande devido a que permite medir intervalos de tempo tão grandes como seja possível. */