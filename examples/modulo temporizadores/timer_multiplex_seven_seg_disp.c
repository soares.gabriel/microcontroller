/*
Exercício 3. Projetar um sistema que faça a contagem de 0 até 9999.
Ou seja, que quando se ligue o kit este comece a contar desde 0000 até
9999 em intervalos de tempo de 0,25s; Fazer isto em quatro displays de
7 segmentos.
*/


unsigned int i, num_1, num_2, nDisplays = (4 - 1), displaysCounter = 0, D0, D1, D2, D3;
char displaysCtrl;


unsigned int mask(unsigned int num){
        switch(num){
                case 0 : return 0x3F;
                case 1 : return 0x06;
                case 2 : return 0x5B;
                case 3 : return 0x4F;
                case 4 : return 0x66;
                case 5 : return 0x6D;
                case 6 : return 0x7D;
                case 7 : return 0x07;
                case 8 : return 0x7F;
                case 9 : return 0x6F;
        }
}


void Timer1Int() iv IVT_ADDR_T1INTERRUPT ics ICS_AUTO
{
        IFS0bits.T1IF = 0;

        displaysCounter++;
        switch (displaysCounter) {
                case 1:
                        LATDbits.LATD0 = 1;
                        LATDbits.LATD2 = 0;
                        LATCbits.LATC13 = 0;
                        LATCbits.LATC14 = 0;
                        LATB = D0;
                        break;
                case 2:
                        LATDbits.LATD0 = 0;
                        LATDbits.LATD2 = 1;
                        LATCbits.LATC13 = 0;
                        LATCbits.LATC14 = 0;
                        LATB = D1;
                        break;
                case 3:
                        LATDbits.LATD0 = 0;
                        LATDbits.LATD2 = 0;
                        LATCbits.LATC13 = 1;
                        LATCbits.LATC14 = 0;
                        LATB = D2;
                        break;
                case 4:
                        LATDbits.LATD0 = 0;
                        LATDbits.LATD2 = 0;
                        LATCbits.LATC13 = 0;
                        LATCbits.LATC14 = 1;
                        LATB = D3;
                        displaysCounter = 0;
                        break;
        }
}

int main (void)
{
        ADPCFG = 0xFFFF;

        TRISB = 0; // a PORTB como saída
        LATB = 0;

        TRISDbits.TRISD0 = 0;
        TRISDbits.TRISD2 = 0;
        TRISCbits.TRISC13 = 0;
        TRISCbits.TRISC14 = 0;

        IFS0 = 0; //Flag de interrupção do timer1
        IEC0bits.T1IE = 1;
        PR1 = 32000;
        T1CON = 0x8000; //ativamos o timer1 e o Prescaler fica em 1

        do
        {
                for(i = 0; i <= 9999; i++){
                        num_2 = 10;   // loop para visualização
                        while(num_2 != 0){

                                // milhares
                                num_1 = i/1000;
                                D0 = mask(num_1);

                                // centenas
                                num_1 = i%1000;
                                num_1 = num_1/100;
                                D1 = mask(num_1);

                                // dezenas
                                num_1 = i%100;
                                num_1 = num_1/10;
                                D2 = mask(num_1);

                                // unidades
                                num_1 = i%10;
                                D3 = mask(num_1);

                                num_2--;
                        }
                }
        } while(1);
}
