/*
Escrever um programa usando o Timer 1 para criar uma onda 
quadrada de 10 KHz no pino 0 da porta B. Sabendo que a
frequência do cristal é de 8MHz, se utiliza o PLL x8 e 
o prescaler do timer é 1. Usar interrupções (Timer1).
*/

void __attribute__ ((interrupt, no_auto_psv)) _T1Interrupt(void)
{
	IFS0bits.T1IF = 0;
	LATBbits.LATB0 = ~LATBbits.LATB0; //Complementamos o bit.
}

//************* Programa Principal *************************
void main (void)
{
	ADPCFG = 0xFFFF;
	//configura a porta B (PORTB) como entradas/saidas digitais
	TRISB=0;
	//a PORTB como saída
	IFS0=0;
	//Flag de interrupção do timer1
	LATB=0;
	IEC0 = IEC0 | 0x0008; // bit 3 do registrador IEC0 habilita a interrupção do timer1 (IEC0bits.T1IE=1)
	PR1 = 800; //O registrador de periodo PR1 é igual a 800
	T1CON=0x8000; //ativamos o timer1 e o Prescaler fica em 1.
	while(1); //laço infinito
}