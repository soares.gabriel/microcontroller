// conexoes do modulo LCD
sbit LCD_RS at LATE0_bit;
sbit LCD_EN at LATE1_bit;
sbit LCD_D4 at LATE2_bit;
sbit LCD_D5 at LATE3_bit;
sbit LCD_D6 at LATE4_bit;
sbit LCD_D7 at LATE5_bit;
sbit LCD_RS_Direction at TRISE0_bit;
sbit LCD_EN_Direction at TRISE1_bit;
sbit LCD_D4_Direction at TRISE2_bit;
sbit LCD_D5_Direction at TRISE3_bit;
sbit LCD_D6_Direction at TRISE4_bit;
sbit LCD_D7_Direction at TRISE5_bit;
// Fim das conexoes do modulo LCD

//Projetar um sistema que mostre nos displays de 7 segmentos a palavra HOLA.
//Declara��o de vari�veis globais
float tensao = 0, tensaoAVG = 0;
char tensaoSTR[15]; //Utilizada para receber a convers�o da tens�o de float para str.
char tensaoAVG_STR[15];
int convertido = 0, i = 0, contAVG = 0, exibeAVG = 0;
//-- Fun��o respons�vel pela configura��o da comunica��o serial --//
//Funciona com uma freq��ncia de 8 MHz e ativa as interrup��es de Tx e Rx
void INIT_UART2(unsigned char valorBaud)
{
    U2BRG = valorBaud;
    /*Configuramos a UART, 8 bits de dados, 1 bit de parada, sem paridade */
    U2MODE = 0x0000; //Ver tabela para saber as outras configura��es
    U2STA = 0x0000;
    IPC2 = 0x0440; //A faixa de prioridade m�dia, n�o � urgente.
    IFS1bits.U2TXIF = 0; //Zerar o flag de interrup��o de Tx.
    IEC1bits.U2TXIE = 0; //Habilita interrup��o de Tx.
    IFS1bits.U2RXIF = 0; //Zerar o flag de interrup��o de Rx.
    IEC1bits.U2RXIE = 0; //Habilita interrup��o de Rx.
    U2MODEbits.USIDL = 1;
    U2MODEbits.UARTEN = 1; //E liga a UART
    U2STAbits.UTXEN = 1;
}

//-- Fun��o respons�vel pelo envio de um caractere de sa�da pela porta serial --//
void OUTCHR_UART2(unsigned char c)
{
    while (U2STAbits.UTXBF); //Espera enquanto o buffer de Tx est� cheio.
    U2TXREG = c; //Escreve o caractere.
}

//-- Fun��o respons�vel pelo envio de um vetor de char de sa�da pela porta serial --//
void OUTSTR_UART2(unsigned char *s)
{
    i = 0;
    while(s[i]) //La�o at� *s == �\0�, ou seja, o fim da string.
    OUTCHR_UART2(s[i++]); //Envia o caractere e fica pronto para enviar o pr�ximo.
}

void init_ADC_man() //masc = mascara para selecionar os pinos que ser�o analogicos
{
    //ADPCFG = masc; // seleciona pinos de entrada anal�gicos
    ADCON1 = 0; // controle de sequencia de convers�o manual
    ADCSSL = 0; // n�o � requerida a varredura ou escaneamento
    ADCON2 = 0; // usa MUXA, AVdd e AVss s�o usados como Vref+/-
    ADCON3 = 0x0007; // Tad = 4 x Tcy = 4* 62,5ns = 250 ns > 153,85 ns (quando Vdd = 4,5 a 5,5V); SAMC = 0 (n�o � levado em considera��oquando � convers�o manual)
    ADCON1bits.ADON = 1; // liga o ADC
} //init_ADC_man
//Rotina basica de leitura (amostragem manual) do conversor A/D

int ler_ADC_man(int canal)
{
    ADCHS = canal; // seleciona canal de entrada anal�gica
    ADCON1bits.SAMP = 1; // come�a amostragem
    Delay_us(10); //tempo de amostragem
    ADCON1bits.SAMP = 0; // come�a a convers�o
    while (!ADCON1bits.DONE); // espera que complete a convers�o
    return ADCBUF0; // le o resultado da convers�o.
}

void interrupcao() iv IVT_ADDR_T1Interrupt
{
  IFS0bits.T1IF = 0;
  OUTSTR_UART2("Tens�o: ");
  OUTSTR_UART2(tensaoSTR);
  OUTSTR_UART2("\r\n");
  Lcd_Cmd(_LCD_CLEAR); // Limpa o LCD
  Lcd_Out(1,1,"V: "); // Mostra valor da tens�o no LCD
  Lcd_Out(1,4,tensaoSTR); // Mostra valor da tens�o no LCD
  if(exibeAVG == 1)
  {
     OUTSTR_UART2("Tens�o m�dia: ");
     OUTSTR_UART2(tensaoAVG_STR);
     OUTSTR_UART2("\r\n");
     exibeAVG = 0;
     Lcd_Out(2, 1, "Vm: "); // Mostra valor da tens�o no LCD
     Lcd_Out(2, 5, tensaoAVG_STR); // Mostra valor da tens�o no LCD
  }



}

void interrupcaoT2() iv IVT_ADDR_T2Interrupt
{
  IFS0bits.T2IF = 0;
  convertido = ler_ADC_man(8); //Ler o valor convertido na entrada anal�gica do ADC
  tensao = (convertido*5.0)/(1023.0);
  FloatToStr(tensao, tensaoSTR);
  if(contAVG > 9)
  {
     contAVG = 0;
     tensaoAVG = tensaoAVG/(10.0);
     FloatToStr(tensaoAVG,tensaoAVG_STR); // Converte a vari�vel float tensao para string, atribuindo o valor na vari�vel tensaoSTR.
     tensaoAVG = 0;
     exibeAVG = 1;
  }
  else
  {
     tensaoAVG += tensao;
     contAVG++;
  }


}

void main()
{
        ADPCFG = 0xFEFF; // programa todas os pinos do portb como I/O de uso geral
        TRISBbits.TRISB0 = 0;
        TRISBbits.TRISB8 = 1;
        init_ADC_man();
        IFS0=0; //Flag de interrup��o do timer1
        IEC0 = IEC0 | 0x0008; // bit 3 do registrador IEC0 habilita a interrup��o do timer1 (IEC0bits.T1IE=1)
        PR1 = 62500;
        T1CON=0x8030; //ativamos o timer1 e o Prescaler fica em 1.
        IEC0bits.T2IE = 1; // bit 3 do registrador IEC0 habilita a interrup��o do timer1 (IEC0bits.T1IE=1)
        PR2 = 2000;
        T2CONbits.TON = 1;
        Lcd_Init();// Inicializa LCD
        Lcd_Cmd(_LCD_CLEAR); // Limpa o LCD
        Lcd_Cmd(_LCD_CURSOR_OFF); // Cursor off
        INIT_UART2(103);
        OUTSTR_UART2("Iniciei!");
        while(1);


}
