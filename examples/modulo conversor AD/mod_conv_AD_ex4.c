/*
Conversao A/D com dois potenciometros ligados a RB8 e RB7. 
Os valores de cada um serão mostrados no LCD. O tempo de amostragem 
será definido pelo usuário. O tempo de conversão será de 12 TAD para 
cada entrada analógica.
*/

// conexoes do modulo LCD
sbit LCD_RS at LATE0_bit;
sbit LCD_EN at LATE1_bit;
sbit LCD_D4 at LATE2_bit;
sbit LCD_D5 at LATE3_bit;
sbit LCD_D6 at LATE4_bit;
sbit LCD_D7 at LATE5_bit;
sbit LCD_RS_Direction at TRISE0_bit;
sbit LCD_EN_Direction at TRISE1_bit;
sbit LCD_D4_Direction at TRISE2_bit;
sbit LCD_D5_Direction at TRISE3_bit;
sbit LCD_D6_Direction at TRISE4_bit;
sbit LCD_D7_Direction at TRISE5_bit;
// Fim das conexoes do modulo LCD

float tensao = 0;
char tensaoSTR[15]; //Utilizada para receber a conversão da tensão de float para str.
int convertido = 0;

void init_ADC_man() //masc = mascara para selecionar os pinos que serão analogicos
{
    //ADPCFG = masc; // seleciona pinos de entrada analógicos
    ADCON1 = 0; // controle de sequencia de conversão manual
    ADCSSL = 0; // não é requerida a varredura ou escaneamento
    ADCON2 = 0; // usa MUXA, AVdd e AVss são usados como Vref+/-
    ADCON3 = 0x0007; // Tad = 4 x Tcy = 4* 62,5ns = 250 ns > 153,85 ns (quando Vdd = 4,5 a 5,5V); SAMC = 0 (não é levado em consideraçãoquando é conversão manual)
    ADCON1bits.ADON = 1; // liga o ADC
} //init_ADC_man
//Rotina basica de leitura (amostragem manual) do conversor A/D


int ler_ADC_man(int canal)
{
    ADCHS = canal; // seleciona canal de entrada analógica
    ADCON1bits.SAMP = 1; // começa amostragem
    delay_us(10); //tempo de amostragem
    ADCON1bits.SAMP = 0; // começa a conversão
    while (!ADCON1bits.DONE); // espera que complete a conversão
    return ADCBUF0; // le o resultado da conversão.
}

// ------------------------ PROGRAMA PRINCIPAL--------------------------------------------//
int main(void)
{
    ADPCFG = 0xFE7F; // Pino RB8 e RB7 como entrada analógica (0 = analógica, 1=digital)
    TRISB = 0xFFFF; // Porta B como entrada
    Lcd_Init();// Inicializa LCD
    Lcd_Cmd(_LCD_CLEAR); // Limpa o LCD
    Lcd_Cmd(_LCD_CURSOR_OFF); // Cursor off
    init_ADC_man();
    while(1)
    {
        convertido = ler_ADC_man(7); //Ler o valor convertido na entrada analógica do ADC
        tensao = (convertido*5.0)/1023;
        FloatToStr(tensao,tensaoSTR); // Converte a variável float tensao para string, atribuindo o valor na variável tensaoSTR.
        Lcd_Cmd(_LCD_CLEAR); // Limpa o LCD
        Lcd_Out(1,1,tensaoSTR); // Mostra valor da tensão no LCD
        convertido = ler_ADC_man(8); //Ler o valor convertido na entrada analógica do ADC
        tensao = (convertido*5.0)/1023;
        FloatToStr(tensao,tensaoSTR); // Converte a variável float tensao para string, atribuindo o valor na variável tensaoSTR.
        Lcd_Out(2,1,tensaoSTR); // Mostra valor da tensão no LCD
        delay_ms(1000);
    }
}