/* 
Conversão **MANUAL** A/D com um potenciometro ligado a RB8. Os valores serão mostrados 
no LCD. O tempo de amostragem será definido pelo usuário. O tempo de conversão será 
de 12 TAD para cada entrada analógica. 
*/

// conexoes do modulo LCD
sbit LCD_RS at LATE0_bit;
sbit LCD_EN at LATE1_bit;
sbit LCD_D4 at LATE2_bit;
sbit LCD_D5 at LATE3_bit;
sbit LCD_D6 at LATE4_bit;
sbit LCD_D7 at LATE5_bit;
sbit LCD_RS_Direction at TRISE0_bit;
sbit LCD_EN_Direction at TRISE1_bit;
sbit LCD_D4_Direction at TRISE2_bit;
sbit LCD_D5_Direction at TRISE3_bit;
sbit LCD_D6_Direction at TRISE4_bit;
sbit LCD_D7_Direction at TRISE5_bit;
// Fim das conexoes do modulo LCD

float tensao = 0;
char tensaoSTR[15]; //Utilizada para receber a conversão da tensão de float para str.
int convertido = 0;

void main()
{
    //--------------------- CONFIGURAÇÃO DE ENTRADA/SAÍDA ------------------------------------//
    ADPCFG = 0xFEFF; // Pino RB8 como entrada analógica (0 = analógica, 1=digital)
    TRISB = 0xFFFF; // Porta B como entrada
    //-------------------------- CONFIGURAÇÃO DO CONVERSOR A/D -------------------------------//
    //Configuração do SFR ADCON1:Adc desligado, Formato de saída: inteiro, Amostragem/conversão manual
    ADCON1 = 0;
    //Configuração do SRF ADCON2: tensão de referência: AVDD e AVSS, sem varredura, conversão pelo canal 0, interrupção após uma amostra buffer como palavra de 16 bits
    ADCON2 = 0;
    //Configuração do SRF ADCON3: para o clock do ADC de 500kHz, tempo de auto-amostragem = 0 TAD, fonte do clock: ciclo da máquina
    ADCON3 = 0x0007; //Tad= 4 * Tcy = 4*62,5 ns = 250 ns > 153,85 ns (quando Vdd = 4,5 a 5,5V)
    // ADCON3 = 0x000B; // Tad = 6 * Tcy = 6* 62,5ns = 375ns >256,41ns (quando Vdd = 3 a 5,5V);
    //Configuração do SRF ADCHS: seleciona o canal CH0, configura entrada analógica AN8 (RB8), entrada de referência negativa do
    //CHO igual a VrefADCHS=0x0000;
    ADCHSbits.CH0SA = 8; // seleciona a entrada analogica 8
    //Configuração do SRF ADCSSL: varredura desativada
    ADCSSL=0;
    ADCON1bits.ADON = 1; //Ativa o ADC
    // ------------------------ CONFIGURAÇÃO DO LCD ---------------------------------------//
    Lcd_Init();// Inicializa LCD
    Lcd_Cmd(_LCD_CLEAR); // Limpa o LCD
    Lcd_Cmd(_LCD_CURSOR_OFF); // Cursor off
    
    while(1)
    {
        ADCON1bits.SAMP = 1; //Inicia a amostragem
        
        Delay_us(10); //Espera a amostragem
        
        ADCON1bits.SAMP = 0; //Para a amostragem e inicia a conversão
        
        while(!ADCON1bits.DONE); //Aguarda o fim da conversão
        
        convertido = ADCBUF0; //Ler o valor convertido no canal 0 do ADC
        tensao = (convertido*5.0)/1023; // Cálculo da tensão de entrada
        
        FloatToStr(tensao,tensaoSTR); // Converte a variável float tensao para string, atribuindo o valor na variável tensaoSTR.
        Lcd_Cmd(_LCD_CLEAR); //Limpa o LCD.
        Lcd_Out(1, 1, tensaoSTR); //Imprime a variável tensaoSTR4 no display LCD.
        
        Delay_ms(1000);
    }
}