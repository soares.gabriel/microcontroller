# ELET0077 - Microcontroladores ![](https://github.com/llucasmendes/microcontroladores/blob/master/resources/img/mc.png) 

Este repositório contém os arquivos das atividades desenvolvidas na disciplina de Microcontroladores - 2017.2 - T01. O conteúdo desta disciplina envolve uma introdução aos microcontroladores, em especial o dsPIC30F4011 da família [dsPIC30F](http://www.microchip.com/design-centers/16-bit/products/dspic30f), suas características, pinagem, organização da memória, portas de entrada e saída. Bem como, a linguagem C para o dsPIC30F4011 e a integração do seu sistema: oscilador, reset, watchdog timer, modo de economia de potência, proteção de código, Input e Output capture, porta serial, conversor AD, entre outros.

## Ambiente de Desenvolvimento

### Hardware

* [dsPIC30F Motor Control 16-bit Digital Signal Controller](http://www.microchip.com/wwwproducts/en/dsPIC30F4011)
  * [Datasheet](http://ww1.microchip.com/downloads/en/devicedoc/70135C.pdf)
  * [Reference Manual](http://ww1.microchip.com/downloads/en/DeviceDoc/70046E.pdf)
* [PICkit™ 3 In-Circuit Debugger/Programmer](http://www.microchip.com/Developmenttools/ProductDetails.aspx?PartNO=PG164130)
  * [User's Guide](http://ww1.microchip.com/downloads/en/DeviceDoc/52116A.pdf)
  * [Quick Start Guide](http://ww1.microchip.com/downloads/en/DeviceDoc/50002010B.pdf)

### Software

* [mikroC PRO for dsPIC](https://shop.mikroe.com/mikroc-dspicpic24)
  * [User Manual](http://download.mikroe.com/documents/compilers/mikroc/dspic/mikroc-dspic-manual-v100.pdf)
  * [Quick Start Guide](http://download.mikroe.com/documents/compilers/mikroc/dspic/mikroc-dspic-creating-first-project-v100.pdf)
* [PICkit™ 3 Programmer Application](http://ww1.microchip.com/downloads/en/DeviceDoc/PICkit3%20Programmer%20Application%20v3.10.zip)

## Esquemas

### Circuito básico para as atividades práticas

<p align="center">
  <img src="https://github.com/llucasmendes/microcontroladores/blob/master/resources/img/kit_dspic30f4011-5_schematic.png" width="750px" height="500px"/></p>

### Conexões com o microcontrolador para efetuar a gravação do programa

<p align="center">
  <img src="https://github.com/llucasmendes/microcontroladores/blob/master/resources/img/pickit3_connections_dsPIC30F4011.png" width="350px" height="350px"/></p>

## Autores

* **Gabriel Soares** - [golf-sierra](https://github.com/golf-sierra)
* **Lucas Mendes** - [llucasmendes](https://github.com/llucasmendes)
* **Prof. Dr. Antonio Ramirez Hidalgo** - [ahidalgo](mailto:ahidalgo@ufs.br)

## Licença

Este projeto é licenciado de acordo com a licença MIT - vide [LICENSE.md](LICENSE.md) para mais detalhes
