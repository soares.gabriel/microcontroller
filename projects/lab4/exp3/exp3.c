//LABORAT�RIO 04 - MICROCONTROLADORES
//ALUNOS: Lucas Mendes e Gabriel An�sio

// conexoes do modulo LCD
sbit LCD_RS at LATE0_bit;
sbit LCD_EN at LATE1_bit;
sbit LCD_D4 at LATE2_bit;
sbit LCD_D5 at LATE3_bit;
sbit LCD_D6 at LATE4_bit;
sbit LCD_D7 at LATE5_bit;
sbit LCD_RS_Direction at TRISE0_bit;
sbit LCD_EN_Direction at TRISE1_bit;
sbit LCD_D4_Direction at TRISE2_bit;
sbit LCD_D5_Direction at TRISE3_bit;
sbit LCD_D6_Direction at TRISE4_bit;
sbit LCD_D7_Direction at TRISE5_bit;
// Fim das conexoes do modulo LCD

int controleSinal = 0;
int dutyCycleMotor = 0, dutyCycleLeds = 0;
float tensaoM = 0, tensaoL = 0;
float convertido= 0;
int i = 0, j=0;
char tensaoSTR1[4];
char tensaoSTR2[4];

//Interrup��o internar T2 respons�vel por gerar o sinal com o duty cycle de 0 a 100%
void timer1() iv IVT_ADDR_T1Interrupt
{
  IFS0bits.T1IF = 0;
  //Controla o duty cycle da velocidade do motor
  if(controleSinal > dutyCycleMotor)
  {
       LATFbits.LATF0 = 1;
  }
  else
  {
       LATFbits.LATF0 = 0;
  }
  //Controla o duty cycle da intensidade dos leds
  if(controleSinal > dutyCycleLeds)
  {
       LATBbits.LATB0 = 0;
       LATBbits.LATB1 = 0;
       LATBbits.LATB2 = 0;
       LATBbits.LATB3 = 0;
       LATBbits.LATB4 = 0;
       LATBbits.LATB5 = 0;
       LATBbits.LATB6 = 0;
  }
  else
  {
       LATBbits.LATB0 = 1;
       LATBbits.LATB1 = 1;
       LATBbits.LATB2 = 1;
       LATBbits.LATB3 = 1;
       LATBbits.LATB4 = 1;
       LATBbits.LATB5 = 1;
       LATBbits.LATB6 = 1;
  }

  if(controleSinal > 1023)
  {
     controleSinal = 0;
  }
  else
     controleSinal = controleSinal + 1;

}

void init_ADC_man() //masc = mascara para selecionar os pinos que ser�o analogicos
{
    //ADPCFG = masc; // seleciona pinos de entrada anal�gicos
    ADCON1 = 0; // controle de sequencia de convers�o manual
    ADCSSL = 0; // n�o � requerida a varredura ou escaneamento
    ADCON2 = 0; // usa MUXA, AVdd e AVss s�o usados como Vref+/-
    ADCON3 = 0x0007; // Tad = 4 x Tcy = 4* 62,5ns = 250 ns > 153,85 ns (quando Vdd = 4,5 a 5,5V); SAMC = 0 (n�o � levado em considera��oquando � convers�o manual)
    ADCON1bits.ADON = 1; // liga o ADC
} //init_ADC_man
//Rotina basica de leitura (amostragem manual) do conversor A/D

int ler_ADC_man(int canal)
{
    ADCHS = canal; // seleciona canal de entrada anal�gica
    ADCON1bits.SAMP = 1; // come�a amostragem
    Delay_us(122); //tempo de amostragem, com uma frequ�ncia de 8KHz.
    ADCON1bits.SAMP = 0; // come�a a convers�o
    while (!ADCON1bits.DONE); // espera que complete a convers�o
    return ADCBUF0; // le o resultado da convers�o.
}


void main()
{
    ADPCFG = 0xFE7F; // programa os bits 7 e 8 da porta B como anal�gicos e o restante digitais.
    TRISB = 0;
    TRISFbits.TRISF0 = 0; //Sa�da para o MOTOR
    TRISBbits.TRISB8 = 1;   //Pot�nciometro 1 - velocidade do motor
    TRISBbits.TRISB7 = 1;   //Pot�nciometro 2 - intensidade dos leds
    init_ADC_man();
    //Configura��o Timer1 - respons�vel pelo duty cycle
    IEC0bits.T1IE = 1; //Ativa a interrup��o do Timer1.
    PR1 = 62; //Para gerar um PWM de 0-1023 com uma frequ�ncia de aproximadamente 252Hz
    T1CONBits.TON = 1; //Inicia o Timer1.
    Lcd_Init();// Inicializa LCD
    Lcd_Cmd(_LCD_CLEAR); // Limpa o LCD
    Lcd_Cmd(_LCD_CURSOR_OFF); // Cursor off

    while(1)
    {
        //La�o respons�vel pela aquisi��o dos 16 valores das convers�es AD, dos dois potenciometros
        for(j=0; j<16; j++)
        {
           //L� e converte o valor do potenciometro responsavel pelo controle do Motor
           ler_ADC_man(8);
           convertido = ADCBUF0; //Ler o valor do potenciometro respons�vel pelo MOTOR convertido na entrada anal�gica do ADC
           //Incrementa os 16 valores para posteriormente fazer o c�lculo da m�dia
           tensaoM += (float)((convertido*4.0)/1023.0);
           //L� e converte o valor do potenciometro responsavel pelo controle da intensidade dos Leds
           ler_ADC_man(7);
           convertido = ADCBUF0; //Ler o valor do potenciometro respons�vel pelo MOTOR convertido na entrada anal�gica do ADC
           //Incrementa os 16 valores para posteriormente fazer o c�lculo da m�dia
           tensaoL += (float)((convertido*4.0)/1023.0);
        }

        //Calcula as m�dias dos 16 valores e atribui a vari�vel para controlar o duty cycle
        dutyCycleMotor = (tensaoM*255.75) / 16;
        dutyCycleLeds =  (tensaoL*255.75) / 16;
           tensaoM = 0;
        tensaoL = 0;

        //Exibe os valors dos duty cycles no LCD
        IntToStr(dutyCycleMotor, tensaoSTR1);

        Lcd_Out(1,1,"PWM motor: "); // Mostra valor da tens�o no LCD
        Lcd_Out(1,11, tensaoSTR1); // Mostra valor da tens�o no LCD

        IntToStr(dutyCycleLeds, tensaoSTR2);
        Lcd_Out(2,1,"PWM leds: "); // Mostra valor da tens�o no LCD
        Lcd_Out(2,11, tensaoSTR2); // Mostra valor da tens�o no LCD

    }
}