# Experiência 2

Escrever um programa onde o conversor AD será controlado pelo teclado do PC, da seguinte forma: 

* Quando pressionamos a tecla A = aquisição, o conversor converte o sinal analógico de algum dos potenciômetros e vá mostrando esse valor no LCD. 

* Depois pressionando a tecla P = parada, o conversor será parado e não fará a conversão analógica e o PC deverá mostrar a mensagem:

```
a conversão parou, pressionar 'A' para começar de novo
```

Configurar o Conversor AD no modo de amostragem manual. O conversor AD fará 16 conversões a uma frequência de 6KHz e mostrará a média destas no PC. A UART deverá ter um baud rate de 19200.