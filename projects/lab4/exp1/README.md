# Experiência 1

Implementar um programa para um sistema de aquisição de dados com as seguintes características: Este terá um menu, mostrado no PC, onde se poderá selecionar o tempo de aquisição (a cada 0,5 seg, 1 seg, 10 seg, 1 minuto e 1 hora), a aquisição será
manual para as entradas analógicas 8 e 9. Quando se fizer a conversão em algumas das entradas analógicas, se deve fazer 16 conversões na frequência de 8 KHz e tirar a média das tensões destas para serem mostradas no PC e no LCD. A UART deverá ter um baud rate de 19200. Colocar potenciômetros nas entradas analógicas.
