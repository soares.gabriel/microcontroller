
 // LABORATÓRIO 1 QUESTÃO 2 - LUCAS E GABRIEL
#define sw0 PORTDbits.RD1
#define sw1 PORTFbits.RF6

// função que armazena os caracteres a serem exibidos no display
unsigned int mask(unsigned int num) {
  switch (num) {
    case 0 : return 0x3F;
    case 1 : return 0x06;
    case 2 : return 0x5B;
    case 3 : return 0x4F;
    case 4 : return 0x66;
    case 5 : return 0x6D;
    case 6 : return 0x7D;
    case 7 : return 0x07;
    case 8 : return 0x7F;
    case 9 : return 0x6F;
  }
}

//Função que faz o controle de quanto tempo o buzzer ficará ligado e desligado, através da distância.
void controlaBuzzer()
{
     if(d < 20)
     {    LATE = 0b000000;     //PORTA E COMO 1 PARA CONTROLE DOS RELÉS
          LATD = 8; //Ativa o buzzer
          delayBuzzer(1);   //Delay de 0.5s para o buzzer
          //LATEbits.LATE5 = 0x00;    // liga o relé 6
     }
     else if(d >= 20 && d < 50)
     {
          LATE = 0b000001;  //PORTA E COMO 1 PARA CONTROLE DOS RELÉS
          delayBuzzer(1);   //Delay de 0.5s para o buzzer
          LATEbits.LATE4 = 0x00;     //ligar o relé
          //LATFbits.LATF4 = 0x01;    //liga led
     }
     else if(d >= 50 && d < 80)
     {   
        LATE = 0b000011;       //PORTA E COMO 1 PARA CONTROLE DOS RELÉS
         delayBuzzer(2);    //Delay de 0.75s para o buzzer
         //LATEbits.LATE3 = 0x00;
         LATFbits.LATF1 = 0x00;     //liga led
         LATFbits.LATF4 = 0x00;
     }
     else if(d >= 80 && d < 120)
     {
         LATE = 0b000111;      //PORTA E COMO 1 PARA CONTROLE DOS RELÉS
         delayBuzzer(3);   //Delay de 1s para o buzzer
         //LATEbits.LATE2 = 0x00;
         LATFbits.LATF1 = 0x01;       //liga led
     }
     else if(d >= 120 && d < 180)
     {
         LATE = 0b001111;        //PORTA E COMO 1 PARA CONTROLE DOS RELÉS
         delayBuzzer(5);   //Delay de 2s para o buzzer
         LATFbits.LATF0 = 0x00;      //liga led
         //LATEbits.LATE1 = 0x00;
     }
     else if(d >= 180 && d < 200)
     {   
         LATE = 0b011111;
         delayBuzzer(8);  //Delay de 3s para o buzzer
         //LATEbits.LATE0 = 0x00;
         LATFbits.LATF0 = 0x01;      //liga led
     }
     else
     {
          tempo = 0;
          LATD = 0; //Desativa o buzzer
          LATE = 0xFF; //Desliga todos os leds.
     }
}

unsigned int d = 200, num_1, num_2, tempo=0;

//Função que faz o controle do tempo em que o buzzer permanece ligado/desligado, utilizando o tempo de processamento do código.
//A variável 'm' determina o intervalo de tempo liga/desliga.
void delayBuzzer(int m)
{
          //Condição que faz com que o buzzer permaneça desligado até o tempo 'm'.
          if(tempo < m)
          {
               //Condição para que os leds fiquem desligados quando a distância for muito próxima e na próxima condição
               //os leds são ligados, fazendo com que fiquem piscando para alertar a distância muito próxima.
               if(d<20)
                  LATF = 0xFF;
               else
                  LATD = 0; //Desativa o buzzer
          }
          //Se o tempo for igual ao da variável 'm', ele ativa o buzzer, ficando ativo até o tempo for igual a 2*m.
          //Ou seja, isso faz com que o buzzer fique desligado por um tempo 'm',
          // através da condição anterior, e depois ligado por mais um tempo m, através da condição abaixo.
          if(tempo == m)
          {
               if(d<20)
                  LATF = 0x00;
               else
                  LATD = 8; //Ativa o buzzer
          }
          //Depois de o buzzer desligado por um tempo 'm' e ligado por um tempo 'm', volta a desligá-lo e reinicia o tempo,
          //continuando o mesmo controle.
          if(tempo >= (2*m))
          {
             if(d<20)
                  LATF = 0x00;
             else
                 LATD = 0; //Desativa o buzzer

             tempo = 0;
          }
          tempo = tempo + 1;
}



//'cond' serve para exibir a distância enquanto o botão estiver solto(cond = 0) ou quando estiver pressionado (cond = 1).
void exibeDistancia(int aa, int cond)
{
  num_2 = 50;
  if(cond == 0)
  {
     while(num_2 != 0 && sw0 == 1 && sw1 == 1)
    {
          num_1 = aa/1000;
          LATDbits.LATD0 = 1;
          LATB = mask(num_1);
          Delay_ms(2);
          LATDbits.LATD0 = 0;

          num_1 = aa%1000;
          num_1 = num_1/100;
          LATDbits.LATD2 = 1;
          LATB = mask(num_1);
          Delay_ms(2);
          LATDbits.LATD2 = 0;

          num_1 = aa%100;
          num_1 = num_1/10;
          LATCbits.LATC14 = 1;
          LATB = mask(num_1);
          Delay_ms(2);
          LATCbits.LATC14 = 0;

          num_1 = aa%10;
          num_1 = num_1;
          LATCbits.LATC13 = 1;
          LATB = mask(num_1);
          Delay_ms(2);
          LATCbits.LATC13 = 0;

            num_2--;
    }
  }
  else
  {
      while(num_2 != 0 && (sw0 == 0 || sw1 == 0))
      {
            num_1 = aa/1000;
            LATDbits.LATD0 = 1;
            LATB = mask(num_1);
            Delay_ms(2);
            LATDbits.LATD0 = 0;

            num_1 = aa%1000;
            num_1 = num_1/100;
            LATDbits.LATD2 = 1;
            LATB = mask(num_1);
            Delay_ms(2);
            LATDbits.LATD2 = 0;

            num_1 = aa%100;
            num_1 = num_1/10;
            LATCbits.LATC14 = 1;
            LATB = mask(num_1);
            Delay_ms(2);
            LATCbits.LATC14 = 0;

            num_1 = aa%10;
            num_1 = num_1;
            LATCbits.LATC13 = 1;
            LATB = mask(num_1);
            Delay_ms(2);
            LATCbits.LATC13 = 0;

              num_2--;
      }
  }

}

//Atualiza o valor da distancia, aumentando ou diminuindo, a depender do botão pressionado.
void atualizaDistancia()
{
   if(sw0 == 0)
   {
       while(sw0 == 0)
        {
           exibeDistancia(d, 1);  //Mantem o display ligado ao pressionar o botão SW0
        }
        Delay_ms(20);
        if(d > 2)
            d = d - 10;
   }
   else
   {
        while(sw1 == 0)
        {
           exibeDistancia(d,1);  //Mantem o display ligado ao pressionar o botão SW1
        }
        Delay_ms(20);
        if(d < 400)
            d = d + 10;
   }
}

void main() 
{

   //configuração
   ADPCFG = 0xFFFF; // programa todas os pinos do portb como I/O de uso geral
   TRISB  = 0;
   LATB  = 0;
   TRISC  = 0;
   LATC  = 0;
   TRISD  = 2; //RD1 como entrada (SW0)
   LATD  = 0;
   TRISF.RF0 = 0;
   TRISF.RF1 = 0;
   TRISF.RF4 = 0;
   TRISF = 64; //RF6 como entrada (SW1)

   TRISE = 0; //RE0-RE5 como saída
   //configuração

   while(1)
   {     
   
       exibeDistancia(d, 0);
       while(sw0 == 0 || sw1 == 0)
       {
            atualizaDistancia();
            exibeDistancia(d,0);
       }
       controlaBuzzer();
   }
}