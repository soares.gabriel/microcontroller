# Experiência 1

Usando as teclas SW0 e SW1 e os displays de 7 segmentos será implementado um sistema que deve iniciar mostrando os caracteres “----” e a medida que o usuário esteja digitando a senha os “-” devem ser substituídos pelos respectivos caracteres. Você procura o caractere com a tecla SW0 e a tecla SW1 confirma esse caractere fazendo com que o buzzer apite, ou seja, depois de ter escolhido o caractere este será armazenado e depois é que o buzzer apita. 

Os números serão deslocados de direita para esquerda, como em uma calculadora. Este sistema tem um código interno “secreto” que são os caracteres "UFS145", que terá que ser descoberto pelo usuário de tal forma que:

1. Quando se digitam os caracteres "UFS145" (só nessa ordem U, F,..., 1, 4,...) o código secreto, devem ser mostrados os caracteres digitados se deslocando de direita para esquerda e depois do último digito mostrar a palavra “dSPIC30F4011” deslocando-se para a  esquerda tendo 2 espaços vazios antes de cada repetição. Sinal de que está correto o número digitado.

2. Se digitarem os caracteres diferentes de UFS145 (a verificação dos caracteres deve ser depois de entrarem todos eles), se devem mostrar os caracteres digitados se deslocando de direita para esquerda e depois do último digito mostrar a palavra “ErO” piscando a intervalos de 0,5 seg aproximadamente.

3. Se digitar três vezes os caracteres errados deverá ser ativado o buzzer durante 5 segundos e novamente fica habilitado para entrar o número. 

4. Se deve criar alguma técnica de parada para os itens (1) e (2) de tal forma que possamos executar algum dos outros itens.

**Os caracteres serão reconhecidos depois de soltar a tecla.**
