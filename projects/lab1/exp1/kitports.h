/* Leds */
#define LED0 LATBbits.LATB0
#define LED1 LATBbits.LATB1
#define LED2 LATBbits.LATB2
#define LED3 LATBbits.LATB3
#define LED4 LATBbits.LATB4
#define LED5 LATBbits.LATB5
#define LED6 LATBbits.LATB6
#define LED7 LATBbits.LATB7

/* Displays 7 segmentos */
#define DISP0 LATCbits.LATC13
#define DISP1 LATCbits.LATC14
#define DISP2 LATDbits.LATD2
#define DISP3 LATDbits.LATD0

/* Push buttons */
#define SW0 PORTDbits.RD1
#define SW1 PORTFbits.RF6

/* Buzzer */
#define BZ1 LATDbits.LATD3