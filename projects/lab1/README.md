# Laboratório 1

## Introdução
Nesta prática de laboratório se aplicará o aprendido na sala de aula, ao respeito do Compilador MikroC para dsPIC e do programador do dsPIC30F (PicKit3), assim como também a utilização dos pinos do microcontrolador dsPIC30F4011.

## Objetivos

1. Que o aluno se familiarize com alguns periféricos conectados ao dsPIC30F
2. Aprender a utilizar as teclas, displays de sete segmentos e outros periféricos do kit
3. Utilizar os conhecimentos básicos da linguagem C para o dsPIC em experiências motivadoras
4. Ser criativo na implementação das experiências


