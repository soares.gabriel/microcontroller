/*
# Experi�ncia 1

Controlar a intensidade dos leds ligados � porta B pelo teclado do PC. Depois de digitar a
palavra �alto� (o microcontrolador deve reconhecer todas as letras da palavra nessa ordem )
v�rias vezes a intensidade dos leds vai aumentando at� que chega ao m�ximo ( Digitando 10
vezes a palavra �alto�) e em qualquer momento que digitar a palavra �baixo� (o
microcontrolador deve reconhecer todas as letras da palavra nessa ordem) a intensidade dos
leds vai diminuindo at� que ficam apagados (Digitando 10 vezes a palavra �baixo�). Usar os
par�metros que forem necess�rios.

*/

/* Configura��o do LCD */
sbit LCD_RS at LATE0_bit;
sbit LCD_EN at LATE1_bit;
sbit LCD_D4 at LATE2_bit;
sbit LCD_D5 at LATE3_bit;
sbit LCD_D6 at LATE4_bit;
sbit LCD_D7 at LATE5_bit;

sbit LCD_RS_Direction at TRISE0_bit;
sbit LCD_EN_Direction at TRISE1_bit;
sbit LCD_D4_Direction at TRISE2_bit;
sbit LCD_D5_Direction at TRISE3_bit;
sbit LCD_D6_Direction at TRISE4_bit;
sbit LCD_D7_Direction at TRISE5_bit;

/* declara��o das vari�veis */
unsigned char baudRate = 0, m = 0, updateAllowed = 0;
unsigned char message[15], dutyCycleStr[7];
int dutyCycle = 0;
int control = 0, counter, size = 0;
int i = 0, j = 0, k = 0, l = 0;


/**
 * Interrup��o de RX da UART2
 */
void U2RXInterrupt() iv IVT_ADDR_U2RXInterrupt
{
    IFS1bits.U2RXIF = 0;	// limpa flag de interrup��o
    m = U2RXREG;			// insere caractere recebido na viari�vel m
    if(m == '\n') 			// compara se caractere recebido � igual ao final de linha
    {
        size = i - 1;		// decrementa tamanho da string recebida
        i = 0;				// zera contador
        updateAllowed = 1;	// libera fluxo de compara��o da string recebida
    }
    else
    {
        message[i++] = m; 	//Adiciona um caractere ao vetor de char message[].
    }
}

/**
 * Interrup��o do Timer 2
 */
void T2Interrupt() iv IVT_ADDR_T2INTERRUPT ics ICS_AUTO
{
        IFS0bits.T2IF = 0;		// limpa flag de interrup��o
        if(counter > 10 - dutyCycle)	// duty cycle positivo
            LATB = 0xFFFF;				// porta B em n�vel HIGH
         else						// duty cycle negativo
                 LATB = 0x0000;		// porta B em n�vel LOW


         if(counter > 9)		// resolu��o do duty cycle = 10
            counter = 1;
         else
                 counter++;
}



void INIT_UART2(unsigned char baudRate)
{
    U2BRG = baudRate; //Configuramos a UART com 8 bits de dados, 1 bit de parada e sem paridade.
    U2MODE = 0x0000;
    U2STA = 0x0000;
    IPC2 = 0x0440; //Define a faixa de prioridade como m�dia.
    IFS1bits.U2TXIF = 0; //Zera a flag de interrup��o de Tx.
    IEC1bits.U2TXIE = 0; //Interrup��o de Tx desabilitada.
    IFS1bits.U2RXIF = 0; //Zera a flag de interrup��o de Rx.
    IEC1bits.U2RXIE = 1; //Habilita a interrup��o de Rx.
    U2MODEbits.USIDL = 1; //Para a opera��o caso entre em modo ocioso.
    U2MODEbits.UARTEN = 1; //Ativa a UART.
    U2STAbits.UTXEN = 1; //Ativa a transmis�o UART.
}

/**
 * Fun��o de envio individual de caractere pela porta serial
 * @param c caractere a ser enviado
 */
void OUTCHR_UART2(unsigned char c)
{
    while (U2STAbits.UTXBF);// aguarda buffer de TX esvaziar
    U2TXREG = c; 			// insere caractere no registrador de TX
}

/**
 * Fun��o de envio de um vetor de caracteres pela porta serial
 * @param s ponteiro para primeira posi��o do vetor de caracteres
 */
void OUTSTR_UART2(unsigned char *s)
{
    k = 0;				// zera contador
    while(s[k])			// enquanto n�o atingir o final da string ('\0')
    	OUTCHR_UART2(s[k++]);	// chama fun��o de envio individual de caractere e incrementa contador
}


/**
 * Fun��o de compara��o de vetores de caracteres
 * @param  t  tamanho dos vetores de entrada
 * @param  c1 vetor de caracteres 1
 * @param  c2 vetor de caracteres 2
 * @return    caso os vetores sejam id�nticos retorna 0, caso contr�rio retorna 1
 */
int comp(unsigned char* vec1, unsigned char* vec2, int strlen)
{
    if(strlen != size)		// verifica��o de tamanho do vetor com tamanho do vetor recebido (ISR RX)
    {
        return 1; 		// caso verdadeiro sai da fun��o retornando 1
    }
    
    for(j = 0; j < strlen; j++)	// itere enquanto n�o chegar ao final do vetor
    {
        if(vec1[j] != vec2[j]) 	// caso algum caractere seja diferente sai da fun��o retornando 1
        {
             return 1; 
        }
    }
    return 0; 	// caso contr�rio, os vetores de caracteres s�o iguais, sai da fun��o retornando 0
}



int main(void)
{

    Lcd_Init();                        // Initialize LCD
    Lcd_Cmd(_LCD_CLEAR);               // Clear display
    Lcd_Cmd(_LCD_CURSOR_OFF);          // Cursor off

    ADPCFG = 0xFFFF; //Configura a PORTB como sa�da digital.
    TRISB = 0; //Define a PORTB como sa�da.
    
    TRISDbits.TRISD3 = 0; // buzzer output

    INIT_UART2(103);	// inicializa a UART2 com baud rate de 9600 bps
    
    IFS0 = 0;			// limpa flags de interrup��o
    IEC0bits.T2IE = 1;	// configura interru��o do timer 2

    PR2 = 1600;			// configura o timer 2 para gerar um PWM de 0 a 10 com freq. de 1KHz
    T2CON = 0x8000;		// ativa o timer 2 
    

    while (1)
    { 
        if(updateAllowed==1)
        {
            if(comp("led off", message, 7) == 0)	// compara a string recebida com "led off"	
            {
                dutyCycle = 0;			// desativa LEDs
                Lcd_Cmd(_LCD_CLEAR);	// limpa displays
                Lcd_Out(1, 1, "LEDs OFF");	// exibe informa��o que LEDs foram desligados
                Delay_ms(3);	// delay para a realiza��o da atualiza��o do display 
            }

            if(comp("led on", message, 6) == 0)		// compara a string recebida com "led on"	
            {
                dutyCycle = 5;		// ativa LEDs em 50%
                Lcd_Cmd(_LCD_CLEAR);	// limpa display
                Lcd_Out(1, 1, "LEDs OFF");	// exibe informa��o que LEDs foram ligados
                Delay_ms(3);	// delay para a realiza��o da atualiza��o do display     
            }

            if(comp("led alto", message, 8) == 0)		// compara a string recebida com "led alto"	
            {

                if(dutyCycle < 9)	// caso o duty cycle n�o tenha atingido limite
                {
                    dutyCycle++;	// incrementa duty cycle
                }
                Lcd_Cmd(_LCD_CLEAR);	// limpa display
                IntToStr((dutyCycle + 1)*10, dutyCycleStr); // converte valor do duty cycle para caractere
                Lcd_Out(1, 1, "LEDs ON");	// exibe informa��o que os LEDs foram ligados
                Lcd_Out(2, 1, "Duty cycle: ");	// exibe duty cycle dos LEDs
                Lcd_Out(2,11, strcat(dutyCycleStr, "%")); // formata sa�da do duty cycle com '%'
                Delay_ms(3);	// delay para a realiza��o da atualiza��o do display                
            }

            if(comp("led baixo", message, 9) == 0)		// compara a string recebida com "led baixo"	
            {

                if(dutyCycle > 0)	// caso o duty cycle seja maior que 0
                {
                   dutyCycle--;	// decrementa duty cycle
                }
                
                Lcd_Cmd(_LCD_CLEAR);	// limpa display
                IntToStr((dutyCycle + 1)*10, dutyCycleStr);	// converte valor do duty cycle para caractere
                Lcd_Out(1, 1, "LEDs ON");	// exibe informa��o que os LEDs foram ligados
                Lcd_Out(2, 1, "Duty cycle: ");	// exibe duty cycle dos LEDs
                Lcd_Out(2,11, strcat(dutyCycleStr, "%"));	// formata sa�da do duty cycle com '%'
                Delay_ms(3); 	// delay para a realiza��o da atualiza��o do display                  
            }

            if(comp("buzzer on", message, 9) == 0)		// compara a string recebida com "buzzer on"	
            {
               LATDbits.LATD3 = 1;	// ativa buzzer
               Lcd_Cmd(_LCD_CLEAR);	// limpa display
               Lcd_Out(1, 1, "Buzzer ON"); // exibe informa��o que o buzzer foi ligado
               Delay_ms(3);		// delay para a realiza��o da atualiza��o do display                  
            }

            if(comp("buzzer on 500", message, 13) == 0)		// compara a string recebida com "buzzer on 500"	
            {
               LATDbits.LATD3 = 1;		// ativa buzzer
               Lcd_Cmd(_LCD_CLEAR);		// limpa display
               Lcd_Out(1, 1, "Buzzer ON: 0.5s");	// exibe informa��o que o buzzer foi ligado por 0.5 segundo
               Delay_ms(3);		// delay para a realiza��o da atualiza��o do display
               Delay_ms(500);	// aguarda 0.5 seg
               LATDbits.LATD3 = 0;		// deativa buzzer                 
            } 

            if(comp("buzzer on 1000", message, 14) == 0)	// compara a string recebida com "buzzer on 1000"	
            {
               LATDbits.LATD3 = 1;	// ativa buzzer
               Lcd_Cmd(_LCD_CLEAR);		// limpa LCD
               Lcd_Out(1, 1, "Buzzer ON: 1s");	// exibe informa��o que o buzzer foi ligado por 1 segundo
               Delay_ms(3);		// delay para a realiza��o da atualiza��o do display
               Delay_ms(1000);	// aguarda 1 seg
               LATDbits.LATD3 = 0;	// desativa buzzer    
            }                       

            if(comp("buzzer off", message, 10) == 0)	// compara a string recebida com "buzzer off"	
            {
               LATDbits.LATD3 = 0;		// desativa buzzer
               Lcd_Cmd(_LCD_CLEAR);		// limpa LCD
               Lcd_Out(1, 1, "Buzzer OFF");		// exibe informa��o que o buzzer foi desativado
               Delay_ms(3);                     // delay para a realiza��o da atualiza��o do display
            }            

            if(comp("help", message, 4) == 0)	// compara a string recebida com "help"	
            {
                OUTSTR_UART2("Comandos:\r\n > led alto\r\n > led baixo\r\n > led on\r\n > led led off\r\n");	// exibe a lista de comandos v�lidos no terminal do PC
            }

            updateAllowed = 0;	// zera flag de permiss�o de compara��o
        }
    }
}