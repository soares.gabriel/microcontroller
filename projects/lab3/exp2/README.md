# Experiência 2

Vamos fazer um dialogo com o PC e o microcontrolador (uC):

uC: Oi, PC tudo bem?

PC: Tudo bem e você? (tem que ser digitado por você, mostrar na tela do PC)

uC: Por favor digita a palavra “leds” para ligar e desligar os leds a cada 0,5 segundos.

PC: Tá bom. (mostrar na tela do PC e digita a palavra “leds” que será mostrado no PC)

uC: Agora digita a palavra “PWM” para controlar a intensidade do brilho dos leds. (o PWM fará uma varredura de 10% em 10% desde 0 % até 100 % de duty cycle).

PC: sem problema. (digitar a palavra “PWM” e mostrar no PC).

UC: (Depois que começa a fazer varredura indefinidamente o uC responde) obrigado. Por último digita a palavra “PARAR” para que eu possa parar a ação anterior e que o buzzer apite por 3 segundos. Assim nos poderemos começar de novo.

As respostas do PC só serão obedecidas após terem digitado toda a frase.