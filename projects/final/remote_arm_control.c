#define MIN_PULSE_WIDTH     4    // the shortest pulse sent to a servo    800/20000
#define MAX_PULSE_WIDTH     11   // the longest pulse sent to a servo     2200/20000
#define DEFAULT_PULSE_WIDTH 7.5   // default pulse width when servo is attached  1500/20000

# define SERVO_1_PIN LATBbits.LATB0
# define SERVO_2_PIN LATBbits.LATB1
# define SERVO_3_PIN LATBbits.LATB2
# define SERVO_4_PIN LATBbits.LATB3
//# define SERVO_5_PIN LATBbits.LATB4
# define SERVO_5_PIN LATFbits.LATF1          /* saiu da porta B por causa dos potenci�metros */

 // conexoes do modulo LCD
sbit LCD_RS at LATE0_bit;
sbit LCD_EN at LATE1_bit;
sbit LCD_D4 at LATE2_bit;
sbit LCD_D5 at LATE3_bit;
sbit LCD_D6 at LATE4_bit;
sbit LCD_D7 at LATE5_bit;
sbit LCD_RS_Direction at TRISE0_bit;
sbit LCD_EN_Direction at TRISE1_bit;
sbit LCD_D4_Direction at TRISE2_bit;
sbit LCD_D5_Direction at TRISE3_bit;
sbit LCD_D6_Direction at TRISE4_bit;
sbit LCD_D7_Direction at TRISE5_bit;
// Fim das conexoes do modulo LCD


float contador[5], dutyCycle[5], servo_1 = 0.0, servo_2 = 0.0,servo_3 = 0.0, servo_4 = 0.0, servo_5 = 0.0;
int angle, contBotao = 0;
unsigned short pin_port;
unsigned int i;       int start = 0;    unsigned char m;
//************** Fun��o OUTCHR_UART2(char c) ********************
void OUTCHR_UART2(unsigned char c){
     Delay_ms(5);
     while (U2STAbits.UTXBF); // espera enquanto o buffer de Tx est� cheio.
     U2TXREG = c; // escreve caractere.
}

void OUTSTR_UART2(unsigned char * s){
     i=0;
     while(s[i]) // la�o at� *s == �\0�, fim da string
     OUTCHR_UART2(s[i++]); // envia o caractere e pronto para o pr�ximo
}

//Interrup��o para inser��o de caractere no terminal
void Interrupt_U2RX() iv IVT_ADDR_U2RXINTERRUPT{
     IFS1bits.U2RXIF = 0; // zera o flag
     m = U2RXREG;
     OUTCHR_UART2(m);

     switch(m){    //faz a compara��o de cada uma das poss�veis enntradas. O valor de start determinada o que ser� realizado em seguida
           case 'a':
           start = 1;    //habilita a aquisi��o da tens�o por meio de potenci�metro
           break;

           case 's':
           start = 2;  // (CRIATIVIDADE: -> habilita a aquisi��o da temperatura por meio de sensor
           break;

           case 'd':   //Para o processo de aquisi��o
           start = 3;
           break;
           
           case 'f':   //Para o processo de aquisi��o
           start = 4;
           break;
           
           case 'g':   //Para o processo de aquisi��o
           start = 5;
           break;
           
           case 'h':   //Para o processo de aquisi��o
           start = 6;
           break;
           
           case 'j':   //Para o processo de aquisi��o
           start = 7;
           break;
           
           case 'P':   //Para o processo de aquisi��o
           start = 8;
           break;
           
           case 'k':   //Para o processo de aquisi��o
           start = 9;
           break;
           
           case 'l':   //Para o processo de aquisi��o
           start = 10;
           break;
           
           case 'q':   //Para o processo de aquisi��o
           start = 11;
           break;
           
           case 'w':   //Para o processo de aquisi��o
           start = 12;
           break;
           
           case 'e':   //Para o processo de aquisi��o
           start = 13;
           break;
           
           case 'r':   //Para o processo de aquisi��o
           start = 14;
           break;
           
           case 't':   //Para o processo de aquisi��o
           start = 15;
           break;

           default:
           OUTSTR_UART2("\r\n");
           OUTSTR_UART2("Comando inv�lido!!! \r\n");
     }
}
//Inicializacao da Porta UART2
void INIT_UART2 (unsigned char valor_baud)
{
        U2BRG = valor_baud;
        /*Configuramos a UART, 8 bits de dados, 1 bit de parada, sem paridade */
        U2MODE = 0x0000; //Ver tabela para saber as outras configura��es
        U2STA = 0x0000;
        IPC2 = 0x0440; //A faixa de prioridade m�dia, n�o � urgente.
        IFS1bits.U2TXIF = 0; //Zerar o flag de interrup��o de Tx.
        IEC1bits.U2TXIE = 0; //Habilita interrup��o de Tx.
        IFS1bits.U2RXIF = 0; //Zerar o flag de interrup��o de Rx.
        IEC1bits.U2RXIE = 1; //Habilita interrup��o de Rx.
        U2MODEbits.USIDL = 1;
        U2MODEbits.UARTEN = 1; //E liga a UART
        U2STAbits.UTXEN = 1;
}
void T1Interrupt() iv IVT_ADDR_T1INTERRUPT ics ICS_AUTO
{
        IFS0bits.T1IF = 0;
        if(contador[0] > 100 - dutyCycle[0])
             SERVO_1_PIN = 1;
         else
             SERVO_1_PIN = 0;

         if(contador[0] > 99)
            contador[0] = 1;
         else
                 contador[0] += 1;
}

void T2Interrupt() iv IVT_ADDR_T2INTERRUPT ics ICS_AUTO
{
        IFS0bits.T2IF = 0;
        if(contador[1] > 100 - dutyCycle[1])
             SERVO_2_PIN = 1;
         else
             SERVO_2_PIN = 0;

         if(contador[1] > 99)
            contador[1] = 1;
         else
                 contador[1] += 1;
}

void T3Interrupt() iv IVT_ADDR_T3INTERRUPT ics ICS_AUTO
{
        IFS0bits.T3IF = 0;
        if(contador[2] > 100 - dutyCycle[2])
             SERVO_3_PIN = 1;
         else
             SERVO_3_PIN = 0;

         if(contador[2] > 99)
            contador[2] = 1;
         else
                 contador[2] += 1;
}

void T4Interrupt() iv IVT_ADDR_T4INTERRUPT ics ICS_AUTO
{
        IFS1bits.T4IF = 0;
        if(contador[3] > 100 - dutyCycle[3])
             SERVO_4_PIN = 1;
         else
             SERVO_4_PIN = 0;

         if(contador[3] > 99)
            contador[3] = 1;
         else
                 contador[3] += 1;
}

void T5Interrupt() iv IVT_ADDR_T5INTERRUPT ics ICS_AUTO
{
        IFS1bits.T5IF = 0;
        if(contador[4] > 100 - dutyCycle[4])
             SERVO_5_PIN = 1;
         else
             SERVO_5_PIN = 0;

         if(contador[4] > 99)
            contador[4] = 1;
         else
                 contador[4] += 1;
}

void InitMultiServo(){
    IFS0 = 0; //Flag de interrup��o do timer1
    IEC0bits.T1IE = 1;
    IEC0bits.T2IE = 1;
    IEC0bits.T3IE = 1;
    IEC1bits.T4IE = 1;
    IEC1bits.T5IE = 1;
    PR1 = 3200;
    PR2 = 3200;
    PR3 = 3200;
    PR4 = 3200;
    PR5 = 3200;
    T1CON = 0x8000;
    T2CON = 0x8000;
    T3CON = 0x8000;
    T4CON = 0x8000;
    T5CON = 0x8000;
}

void Servo(unsigned short pin, signed int angle){
     pin_port = pin;
     dutyCycle[pin] =  angle*(MAX_PULSE_WIDTH - MIN_PULSE_WIDTH)/180 + MIN_PULSE_WIDTH;
}


void INIT_ADC_MANUAL() //masc = mascara para selecionar os pinos que ser�o analogicos
{
    //ADPCFG = masc; // seleciona pinos de entrada anal�gicos
    ADCON1 = 0; // controle de sequencia de convers�o manual
    ADCSSL = 0; // n�o � requerida a varredura ou escaneamento
    ADCON2 = 0; // usa MUXA, AVdd e AVss s�o usados como Vref+/-
    ADCON3 = 0x0007; // Tad = 4 x Tcy = 4* 62,5ns = 250 ns > 153,85 ns (quando Vdd = 4,5 a 5,5V); SAMC = 0 (n�o � levado em considera��oquando � convers�o manual)
    ADCON1bits.ADON = 1; // liga o ADC
} //INIT_ADC_MANUAL
//Rotina basica de leitura (amostragem manual) do conversor A/D


int READ_ADC_MANUAL(unsigned int canal)
{
    ADCHS = canal; // seleciona canal de entrada anal�gica
    ADCON1bits.SAMP = 1; // come�a amostragem
    Delay_us(125); //tempo de amostragem para 8KHz
    ADCON1bits.SAMP = 0; // come�a a convers�o
    while (!ADCON1bits.DONE); // espera que complete a convers�o
    return ADCBUF0; // le o resultado da convers�o.
}

/* from https://github.com/arduino/Arduino/issues/2466#issuecomment-69873889 */
long map(long x, long in_min, long in_max, long out_min, long out_max)
{
  if ((in_max - in_min) > (out_max - out_min)) {
    return (x - in_min) * (out_max - out_min+1) / (in_max - in_min+1) + out_min;
  }
  else
  {
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
  }
}

void main() {
    ADPCFG = 0xFE0F; // Pinos RB8 a RB4 como entrada anal�gica (0 = anal�gica, 1=digital) (potenci�metros)
    TRISB = 0xFFF0; //  Pinos RB0 a RB3 como sa�da (servos)
    IEC1bits.INT2IE = 1;//Habilita INT0
    TRISFbits.TRISF1 = 0; // Pino RF0 como sa�da (5� servo)
    TRISFbits.TRISF0 = 0; // Pino RF0 como sa�da (5� servo)

    //Inicializando o LCD
     Lcd_Init();
     Lcd_Cmd(_LCD_CLEAR);
     Lcd_Cmd(_LCD_CURSOR_OFF);

    INIT_ADC_MANUAL();
    InitMultiServo();
    INIT_UART2(103);  //incializa comunicacao UART -> velocidade de 19200

     OUTSTR_UART2("--------------------- MENU ---------------------\r\n");
     OUTSTR_UART2("Digite uma letra para comecar a aquisicao \r\n");


    while(1)
    {
        switch (start)
        {
        case 1:
             Servo(0, 180);
             Delay_ms(15);
        break;
        case 2:
             Servo(0, 0);
             Delay_ms(15);
        break;

        case 3:
             Servo(1, 180);
             Delay_ms(15);
        break;
        case 4:
             Servo(1, 0);
             Delay_ms(15);
        break;

        case 5:
             Servo(2, 180);
             Delay_ms(15);
        break;
        case 6:
             Servo(2, 0);
             Delay_ms(15);
        break;
        
        case 7:
             Servo(3, 180);
             Delay_ms(15);
        break;
        case 8:
             Servo(3, 0);
             Delay_ms(15);
        break;
        
        case 9:
             Servo(4, 180);
             Delay_ms(15);
        break;
        case 10:
             Servo(4, 0);
             Delay_ms(15);
        break;
        
        case 11:
             Servo(0, 90);
             Delay_ms(15);
        break;
        
        case 12:
             Servo(1, 90);
             Delay_ms(15);
        break;
        
        case 13:
             Servo(2, 90);
             Delay_ms(15);
        break;
        
        case 14:
             Servo(3, 90);
             Delay_ms(15);
        break;
        
        case 15:
             Servo(4, 90);
             Delay_ms(15);
        break;
        }

    }
}