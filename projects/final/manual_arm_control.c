#define MIN_PULSE_WIDTH     4    // the shortest pulse sent to a servo    800/20000
#define MAX_PULSE_WIDTH     11   // the longest pulse sent to a servo     2200/20000
#define DEFAULT_PULSE_WIDTH 7.5   // default pulse width when servo is attached  1500/20000

# define SERVO_1_PIN LATBbits.LATB0
# define SERVO_2_PIN LATBbits.LATB1
# define SERVO_3_PIN LATBbits.LATB2
# define SERVO_4_PIN LATBbits.LATB3
//# define SERVO_5_PIN LATBbits.LATB4
# define SERVO_5_PIN LATFbits.LATF1          /* saiu da porta B por causa dos potenci�metros */

float contador[5], dutyCycle[5], servo_1 = 0.0, servo_2 = 0.0,servo_3 = 0.0, servo_4 = 0.0, servo_5 = 0.0;
int angle, contBotao = 0;
unsigned short pin_port;
unsigned int i;
/*
void InterrupcaoExt2() iv IVT_ADDR_INT2INTERRUPT
{    
     IFS1bits.INT2IF = 0;
     contBotao = contBotao + 1;

    if(contBotao % 2 == 0)
    { // 90 graus
      for(i=0;i<50;i++)
      {
        LATFbits.LATF1 = 1;
        Delay_us(1500);
        LATFbits.LATF1 = 0;
        Delay_us(18500);
      }
    }else{
       //0 graus
       for(i=0;i<50;i++)
      {
        LATFbits.LATF1 = 1;
        Delay_us(800);
        LATFbits.LATF1 = 0;
        Delay_us(19200);
      }
    }

}
*/

void T1Interrupt() iv IVT_ADDR_T1INTERRUPT ics ICS_AUTO
{
        IFS0bits.T1IF = 0;
        if(contador[0] > 100 - dutyCycle[0])
             SERVO_1_PIN = 1;
         else
             SERVO_1_PIN = 0;

         if(contador[0] > 99)
            contador[0] = 1;
         else
                 contador[0] += 1;
}

void T2Interrupt() iv IVT_ADDR_T2INTERRUPT ics ICS_AUTO
{
        IFS0bits.T2IF = 0;
        if(contador[1] > 100 - dutyCycle[1])
             SERVO_2_PIN = 1;
         else
             SERVO_2_PIN = 0;

         if(contador[1] > 99)
            contador[1] = 1;
         else
                 contador[1] += 1;
}

void T3Interrupt() iv IVT_ADDR_T3INTERRUPT ics ICS_AUTO
{
        IFS0bits.T3IF = 0;
        if(contador[2] > 100 - dutyCycle[2])
             SERVO_3_PIN = 1;
         else
             SERVO_3_PIN = 0;

         if(contador[2] > 99)
            contador[2] = 1;
         else
                 contador[2] += 1;
}

void T4Interrupt() iv IVT_ADDR_T4INTERRUPT ics ICS_AUTO
{
        IFS1bits.T4IF = 0;
        if(contador[3] > 100 - dutyCycle[3])
             SERVO_4_PIN = 1;
         else
             SERVO_4_PIN = 0;

         if(contador[3] > 99)
            contador[3] = 1;
         else
                 contador[3] += 1;
}

void T5Interrupt() iv IVT_ADDR_T5INTERRUPT ics ICS_AUTO
{
        IFS1bits.T5IF = 0;
        if(contador[4] > 100 - dutyCycle[4])
             SERVO_5_PIN = 1;
         else
             SERVO_5_PIN = 0;

         if(contador[4] > 99)
            contador[4] = 1;
         else
                 contador[4] += 1;
}

void InitMultiServo(){
    IFS0 = 0; //Flag de interrup��o do timer1
    IEC0bits.T1IE = 1;
    IEC0bits.T2IE = 1;
    IEC0bits.T3IE = 1;
    IEC1bits.T4IE = 1;
    IEC1bits.T5IE = 1;
    PR1 = 3200;
    PR2 = 3200;
    PR3 = 3200;
    PR4 = 3200;
    PR5 = 3200;
    T1CON = 0x8000;
    T2CON = 0x8000;
    T3CON = 0x8000;
    T4CON = 0x8000;
    T5CON = 0x8000;
}

void Servo(unsigned short pin, signed int angle){
     pin_port = pin;
     dutyCycle[pin] =  angle*(MAX_PULSE_WIDTH - MIN_PULSE_WIDTH)/180 + MIN_PULSE_WIDTH;
}


void INIT_ADC_MANUAL() //masc = mascara para selecionar os pinos que ser�o analogicos
{
    //ADPCFG = masc; // seleciona pinos de entrada anal�gicos
    ADCON1 = 0; // controle de sequencia de convers�o manual
    ADCSSL = 0; // n�o � requerida a varredura ou escaneamento
    ADCON2 = 0; // usa MUXA, AVdd e AVss s�o usados como Vref+/-
    ADCON3 = 0x0007; // Tad = 4 x Tcy = 4* 62,5ns = 250 ns > 153,85 ns (quando Vdd = 4,5 a 5,5V); SAMC = 0 (n�o � levado em considera��oquando � convers�o manual)
    ADCON1bits.ADON = 1; // liga o ADC
} //INIT_ADC_MANUAL
//Rotina basica de leitura (amostragem manual) do conversor A/D


int READ_ADC_MANUAL(unsigned int canal)
{
    ADCHS = canal; // seleciona canal de entrada anal�gica
    ADCON1bits.SAMP = 1; // come�a amostragem
    Delay_us(125); //tempo de amostragem para 8KHz
    ADCON1bits.SAMP = 0; // come�a a convers�o
    while (!ADCON1bits.DONE); // espera que complete a convers�o
    return ADCBUF0; // le o resultado da convers�o.
}

/* from https://github.com/arduino/Arduino/issues/2466#issuecomment-69873889 */
long map(long x, long in_min, long in_max, long out_min, long out_max)
{
  if ((in_max - in_min) > (out_max - out_min)) {
    return (x - in_min) * (out_max - out_min+1) / (in_max - in_min+1) + out_min;
  }
  else
  {
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
  }
}

void main() {
    ADPCFG = 0xFE0F; // Pinos RB8 a RB4 como entrada anal�gica (0 = anal�gica, 1=digital) (potenci�metros)
    TRISB = 0xFFF0; //  Pinos RB0 a RB3 como sa�da (servos)
    IEC1bits.INT2IE = 1;//Habilita INT0
    TRISFbits.TRISF1 = 0; // Pino RF0 como sa�da (5� servo)
    TRISFbits.TRISF0 = 0; // Pino RF0 como sa�da (5� servo)

    INIT_ADC_MANUAL();
    InitMultiServo();

    while(1)
    {
        /* Controla Servo 0 com potenci�metro em RB8 */
        servo_1 = map(READ_ADC_MANUAL(8), 0, 1023, 0, 180);
        servo_2 = map(READ_ADC_MANUAL(7), 0, 1023, 0, 180);
        servo_3 = map(READ_ADC_MANUAL(6), 0, 1023, 0, 180);
        servo_4 = map(READ_ADC_MANUAL(5), 0, 1023, 0, 180);
      servo_5 = map(READ_ADC_MANUAL(4), 0, 1023, 0, 180);
        
        Servo(0, servo_1);
        Delay_ms(15);
        Servo(1, servo_2);
        Delay_ms(15);
        Servo(2, servo_3);
        Delay_ms(15);
        Servo(3, servo_4);
        Delay_ms(15);
        Servo(4, servo_5);
        Delay_ms(15);
    }
}