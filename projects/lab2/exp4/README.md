# Experiência 4


Utilizando duas teclas (uma para escolher o valor de PWM e outra para confirmar) mudar a velocidade dos motores do carrinho. O valor do PWM deverá aparecer nos displays de 7 segmentos (a multiplexação destes deve ser feito com interrupções). 

O funcionamento do  sistema será o seguinte: 

* O sistema começa parado (os displays de 7 segmentos desligados e o carrinho parado);
* Depois de pressionar e soltar (ativar) a tecla de interrupção INT0:
	* Ficam habilitadas as duas teclas para mudar o valor do PWM;
	* Os valores escolhidos devem ser mostrados no display de 7 segmentos (lembrem que os números devem ser deslocados nos displays quando se pressionam as teclas).
* Depois de confirmar o valor do PWM o carrinho deve ser ativado de acordo com o valor definido (valor que deve ser mostrado no display de 7 segmentos). Quando se confirma um novo valor do PWM a velocidade do carrinho também mudará, se o valor do PWM não foi confirmado a velocidade do carrinho continua com o valor do PWM anterior. 

A frequência de operação do carrinho será arbitrariamente determinada. Os valores de PWM serão de 0 a 100% em passos de 1%. 

Pensem em alguma forma de usar o kit de relés com as lâmpadas neste exercício, será parte do funcionamento do circuito. Depois vocês podem usar o kit de relés com as lâmpadas para a criatividade se quiserem. 

Determinar qual é o valor mínimo de duty cycle para o qual o motor começa a girar.