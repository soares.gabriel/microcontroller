# Experiência 3

Projetar um sistema que controle o carrinho do laboratório, isto é que controle a velocidade e direção deste. Que mostre no LCD a direção (direita, esquerda, parado, atrás, frente) à que está indo, a velocidade e a distância até algum objeto.

O controle será da seguinte forma: 
	* O carro começará a se deslocar em uma velocidade mínima;
	* Se ele não encontrar nenhum obstáculo aumentará sua velocidade para média;
	* Se durante um tempo (arbitrariamente determinado) não encontrar algum obstáculo aumentará até sua velocidade máxima;
	* Se em qualquer velocidade ele encontrar um obstáculo a uma distância (arbitrariamente determinada): 
		* Diminui sua velocidade até a mínima;
		* Desvia do obstáculo em alguma direção (arbitrariamente determinada);
		* Continua seu percurso “normal”.