/*

# Experi�ncia 3

Projetar um sistema que controle o carrinho do laborat�rio, isto � que controle
a velocidade e dire��o deste. Que mostre no LCD a dire��o (direita, esquerda,
parado, atr�s, frente) � que est� indo, a velocidade e a dist�ncia at� algum objeto.

O controle ser� da seguinte forma:

        * O carro come�ar� a se deslocar em uma velocidade m�nima;

        * Se ele n�o encontrar nenhum obst�culo aumentar� sua velocidade para m�dia;

        * Se durante um tempo (arbitrariamente determinado) n�o encontrar algum obst�culo
          aumentar� at� sua velocidade m�xima;

        * Se em qualquer velocidade ele encontrar um obst�culo a uma dist�ncia (arbitrariamente
          determinada):

                * Diminui sua velocidade at� a m�nima;
                * Desvia do obst�culo em alguma dire��o (arbitrariamente determinada);
                * Continua seu percurso �normal�.

*/

// LCD module connections
sbit LCD_RS at LATE0_bit;
sbit LCD_EN at LATE1_bit;
sbit LCD_D4 at LATE2_bit;
sbit LCD_D5 at LATE3_bit;
sbit LCD_D6 at LATE4_bit;
sbit LCD_D7 at LATE5_bit;

sbit LCD_RS_Direction at TRISE0_bit;
sbit LCD_EN_Direction at TRISE1_bit;
sbit LCD_D4_Direction at TRISE2_bit;
sbit LCD_D5_Direction at TRISE3_bit;
sbit LCD_D6_Direction at TRISE4_bit;
sbit LCD_D7_Direction at TRISE5_bit;


unsigned int contador  = 0, dutyCycle = 5;
float distance, time, velocity, lastDistance = 0, lastTime = 0;
char timeStr[14], distanceStr[14];

void Timer1Int() iv IVT_ADDR_T1INTERRUPT ics ICS_AUTO
{
        IFS0bits.T1IF = 0;
        time = TMR1*62.5e-9*256;
        distance = (time*340/2)*100;
        TMR1 = 0x0000;
}

/**
 * [T1Interrupt description]
 */
void T2Interrupt() iv IVT_ADDR_T2INTERRUPT ics ICS_AUTO
{
        IFS0bits.T2IF = 0;
        if(contador > 10 - dutyCycle)
            LATBbits.LATB8 = 1;
         else
                 LATBbits.LATB8 = 0;


         if(contador > 9)
            contador = 1;
         else
                 contador++;
}

void dirLeft()
{
        /* left motor (black-red): onForward */
        LATFbits.LATF0 = 1;
        LATFbits.LATF1 = 0;

        /* right motor (yellow-green): onReverse */
        LATFbits.LATF4 = 1;
        LATFbits.LATF5 = 0;

        Lcd_Out(2, 1, "����Esquerda����");
        Delay_ms(3);
}

void dirRight()
{
        /* left motor (black-red): onReverse */
        LATFbits.LATF0 = 0;
        LATFbits.LATF1 = 1;

        /* right motor (yellow-green): onForward */
        LATFbits.LATF4 = 0;
        LATFbits.LATF5 = 1;
        
        Lcd_Out(2, 1, "����Direita�����");
        Delay_ms(3);
}

void dirForward()
{
        /* left motor (black-red): onForward */
        LATFbits.LATF0 = 0;
        LATFbits.LATF1 = 1;

        /* right motor (yellow-green): onForward */
        LATFbits.LATF4 = 1;
        LATFbits.LATF5 = 0;

        Lcd_Out(2, 1, "�����Frente�����");
        Delay_ms(3);
}

void dirReverse()
{
        /* left motor (black-red): onReverse */
        LATFbits.LATF0 = 1;
        LATFbits.LATF1 = 0;

        /* right motor (yellow-green): onReverse */
        LATFbits.LATF4 = 0;
        LATFbits.LATF5 = 1;

        Lcd_Out(2, 1, "������Atras�����");
        Delay_ms(3);
}

void onHold()
{
        /* left motor (black-red): stop */
        LATFbits.LATF0 = 0;
        LATFbits.LATF1 = 0;

        /* right motor (yellow-green): stop */
        LATFbits.LATF4 = 0;
        LATFbits.LATF5 = 0;

        Lcd_Out(2, 1, "�����Parado�����");
        Delay_ms(3);
}


void FloatToStr(float number, char *numberStr){

     int centenas, dezenas, unidades, decimos;

     centenas = number/100;
     dezenas = (number/10)%100;
     unidades = number%10;
     decimos = (number*10)%10;

     numberStr[0] = centenas;
     numberStr[1] = dezenas;
     numberStr[2] = unidades;
     numberStr[3] = '.';
     numberStr[4] = decimos;
}


void main() {
    Lcd_Init();                        // Initialize LCD
    Lcd_Cmd(_LCD_CLEAR);               // Clear display
    Lcd_Cmd(_LCD_CURSOR_OFF);          // Cursor off

    ADPCFG = 0xFFFF;
    TRISB = 1; //a PORTB como sa�da
    //LATBbits.LATB8 = 1;

    TRISF = 0; /* a PORTF como sa�da */
    LATF = 0;  /* zerar a PORTF */

    TRISCbits.TRISC14 = 1; // Echo
    TRISFbits.TRISF6 = 0; // Trigger

    IFS0 = 0; //Flag de interrup��o do timer1
    IEC0bits.T1IE = 1;
    IEC0bits.T2IE = 1;

    PR1 = 0xFFFF;
    PR2 = 1600;
    T1CON = 0x8070; // TGATE
    T2CON = 0x8000; // TGATE


    while(1)
    {
            Lcd_Cmd(_LCD_CLEAR);
            
            LATFbits.LATF6 = 0;
            Delay_us(2);
            LATFbits.LATF6 = 1;
            Delay_us(11);
            LATFbits.LATF6 = 0;

            dirForward();
            
            // velocity = abs(distance - lastDistance)/abs(time - lastTime);
            // lastDistance = distance;
            // lastTime = time;
            
            if (distance >= 2 && distance <= 400){

                //FloatToStr(time, timeStr);
                //Lcd_Out(1,6, timeStr);

                FloatToStr(distance, distanceStr);
                Lcd_Out(1, 1, "Dist:");
                Lcd_Out(1,6, strcat(distanceStr, " cm"));
                Lcd_Out(1,6, distanceStr);

                if (distance >= 2 && distance <= 30){
                        dutyCycle = 10;
                        dirReverse(); Delay_ms(1000);
                        
                        if (rand() % 2 == 0) {
                                dirLeft(); Delay_ms(1000);
                        } else {
                                dirRight(); Delay_ms(1000);
                        }

                        dirForward();
                } else if (distance >= 51 && distance <= 100){
                        dutyCycle = 7;
                } else if (distance >= 51 && distance <= 100){
                        dutyCycle = 8;
                } else {
                        dutyCycle = 10;
                }
             } else {
                    Lcd_Out(2, 1, "Fora de alcance!");
             }
        Delay_ms(1000);
    }
}